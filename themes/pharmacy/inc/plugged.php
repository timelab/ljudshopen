<?php
/**
 * Plugged functions
 * Any functions declared here are overwriting counterparts from a plugin or Storefront core.
 *
 * @package pharmacy
 */

 /**
 * Display Featured Products
 * Hooked into the `homepage` action in the homepage template
 * @since  1.0.0
 * @return void
 */
function storefront_featured_products( $args ) {

	if ( class_exists( 'WooCommerce' ) ) {

		$args = apply_filters( 'storefront_featured_products_args', array(
			'limit' 			=> 4,
			'columns' 			=> 4,
			'orderby'			=> 'date',
			'order'				=> 'desc',
			'title'				=> __( 'Featured Products', 'storefront' ),
			) );

		echo '<section class="pharmacy-product-section pharmacy-featured-products">';

			do_action( 'storefront_homepage_before_featured_products' );

			echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

			echo storefront_do_shortcode( 'featured_products',
				array(
					'per_page' 	=> intval( $args['limit'] ),
					'columns'	=> intval( $args['columns'] ),
					'orderby'	=> esc_attr( $args['orderby'] ),
					'order'		=> esc_attr( $args['order'] ),
					) );

			do_action( 'storefront_homepage_after_featured_products' );

		echo '</section>';

	}
}

/**
 * Adds the description of the category on list of categories.
 * @since 1.0.0
 */
function pharmacy_category_description( $category ) {
	echo '<span class="category-description">' . $category->description . '</span>';
}

function pharmacy_add_category_description() {
	if ( 1 == get_theme_mod( 'pharmacy_category_description' ) ) {
    	add_action( 'woocommerce_after_subcategory', 'pharmacy_category_description', 1 );
    } else {
	    return;
    }
}
add_action( 'woocommerce_after_subcategory', 'pharmacy_add_category_description' );

/**
 * Cart Link
 * Plugged to remove 'items' text.
 * @since  1.0.0
 */
if ( ! function_exists( 'storefront_cart_link' ) ) {
	function storefront_cart_link() {
		?>
			<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php _e( 'View your shopping cart', 'storefront' ); ?>">
				<img src="<?= get_stylesheet_directory_uri(); ?>/assets/img/cart.png" /> <span class="count"><?php echo wp_kses_data( WC()->cart->get_cart_contents_count() );?></span>
			</a>
		<?php
	}
}

if ( ! function_exists( 'storefront_credit' ) ) {
 
  //Function goes here
  function storefront_credit() {
  }
  
}


add_action( 'widgets_init', 'remove_some_widgets', 11 );
function remove_some_widgets() {
	unregister_sidebar( 'footer-bar-1' );
	add_action( 'storefront_before_footer', 'custom_footer_bar_sidebar',  10 );

	if ( ! function_exists( 'custom_footer_bar_sidebar' ) ) {
		function custom_footer_bar_sidebar() {
			register_sidebar( array(
				'name'          => __( 'Footer Bar', 'storefront-footer-bar' ),
				'id'            => 'footer-bar-1',
				'description'   => '',
				'before_widget' => '<section id="%1$s" class="widget block %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			) );
		}
	}
}

add_action( 'init', 'wpdev_170663_remove_parent_theme_stuff', 11 );
function wpdev_170663_remove_parent_theme_stuff() {
	remove_action( 'storefront_before_footer', array(Storefront_Footer_Bar(), 'sfb_footer_bar'),  10 );
	add_action( 'storefront_before_footer', 'custom_footer_bar_html',  10 );

	if ( ! function_exists( 'custom_footer_bar_html' ) ) {
		function custom_footer_bar_html() {
			if ( is_active_sidebar( 'footer-bar-1' ) ) {
				echo '<div class="sfb-footer-bar"><div class="col-full"><div class="footer-widgets col-2">';
					dynamic_sidebar( 'footer-bar-1' );
				echo '</div></div></div>';
			}
		}
	}
}


add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

function change_existing_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'SEK': $currency_symbol = ':-'; break;
     }
     return $currency_symbol;
}

add_action( 'woocommerce_shop_loop_item_title', 'loop_product_description', 15 );

if ( ! function_exists( 'loop_product_description' ) ) {
	function loop_product_description() {
		global $product;

		$wc_product = wc_get_product( $product );

		if ( ! $wc_product ) {
			return false;
		}

		$short_description = $wc_product->get_short_description();

		if ( '' !== $short_description ) {
			echo '<div class="description" itemprop="description">' . wp_kses_post( $short_description ) . '</div>';
		}
	}
}

add_action( 'woocommerce_shop_loop_item_title', 'custom_sale_flash', 16, 3 );
add_action( 'woocommerce_before_single_product_summary', 'custom_sale_flash', 15, 3 );

if ( ! function_exists( 'custom_sale_flash' ) ) {
  function custom_sale_flash() {
    global $post, $product;

    ?>
    <?php if ( get_field( 'acf_product_custom_notice', $product->get_id() ) && get_field( 'acf_product_custom_notice', $product->get_id() )['text'] ) : ?>

    	<span class="onsale visible" style="background-color: <?= get_field( 'acf_product_custom_notice', $product->get_id() )['color'] ?> !important">
    		<?= get_field( 'acf_product_custom_notice', $product->get_id() )['text'] ?>
    	</span>

    <?php endif;
  }
}

/**
 * Change price format from range to "From:"
 *
 * @param float $price
 * @param obj $product
 * @return str
 */
function iconic_variable_price_format( $price, $product ) {
 
    $prefix = sprintf('%s: ', __('Från', 'iconic'));
 
    $min_price_regular = $product->get_variation_regular_price( 'min', true );
    $min_price_sale    = $product->get_variation_sale_price( 'min', true );
    $max_price = $product->get_variation_price( 'max', true );
    $min_price = $product->get_variation_price( 'min', true );
 
    $price = ( $min_price_sale == $min_price_regular ) ?
        wc_price( $min_price_regular ) :
        '<del>' . wc_price( $min_price_regular ) . '</del>' . '<ins>' . wc_price( $min_price_sale ) . '</ins>';
 
    return ( $min_price == $max_price ) ?
        $price :
        sprintf('%s%s', $prefix, $price);
 
}
 
add_filter( 'woocommerce_variable_sale_price_html', 'iconic_variable_price_format', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'iconic_variable_price_format', 10, 2 );

// Place in your theme's functions.php file
// Revert grouped product prices to WooCommerce 2.0 format
add_filter( 'woocommerce_grouped_price_html', 'wc_wc20_grouped_price_format', 10, 2 );
function wc_wc20_grouped_price_format( $price, $product ) {
  $tax_display_mode = get_option( 'woocommerce_tax_display_shop' );
  $child_prices     = array();
  foreach ( $product->get_children() as $child_id ) {
    $child_prices[] = get_post_meta( $child_id, '_price', true );
  }
  $child_prices     = array_unique( $child_prices );
  $get_price_method = 'get_price_' . $tax_display_mode . 'uding_tax';
  if ( ! empty( $child_prices ) ) {
    $min_price = min( $child_prices );
    $max_price = max( $child_prices );
  } else {
    $min_price = '';
    $max_price = '';
  }
  if ( $min_price == $max_price ) {
    $display_price = wc_price( $product->$get_price_method( 1, $min_price ) );
  } else {
    $from          = wc_price( $product->$get_price_method( 1, $min_price ) );
    $display_price = sprintf( __( 'Från: %1$s', 'woocommerce' ), $from );
  }
  return $display_price;
}