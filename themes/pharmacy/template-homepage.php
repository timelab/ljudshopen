<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Homepage
 *
 * @package storefront
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			/**
			 * Functions hooked in to homepage action
			 *
			 * @hooked storefront_homepage_content      - 10
			 * @hooked storefront_product_categories    - 20
			 * @hooked storefront_recent_products       - 30
			 * @hooked storefront_featured_products     - 40
			 * @hooked storefront_popular_products      - 50
			 * @hooked storefront_on_sale_products      - 60
			 * @hooked storefront_best_selling_products - 70
			 */
			do_action( 'homepage' ); ?>

			<section class="ljudshopen-promotion <?= get_field( 'acf_startpage_promotion_width' ); ?>" style="background-image: url('<?= get_field( 'acf_startpage_promotion_background' )['url']; ?>')">
				<div class="text-container <?= get_field( 'textfarg' ); ?>">
					<h3><?= get_field( 'acf_startpage_promotion_subheader' ); ?></h3>
					<h2><?= get_field( 'acf_startpage_promotion_header' ); ?></h2>
					<p><?= get_field( 'acf_startpage_promotion_text' ); ?></p>
					<a rel="nofollow" class="button" href="<?= get_field( 'acf_startpage_promotion_button' )['link'] ?>"><?= get_field( 'acf_startpage_promotion_button' )['text'] ?></a>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
