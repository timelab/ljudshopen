<?php

/**
 * Feed Making View
 *
 * @link       https://webappick.com/
 * @since      1.0.0
 *
 * @package    Woo_Feed
 * @subpackage Woo_Feed/admin/partial
 * @author     Ohidul Islam <wahid@webappick.com>
 */


$dropDown = new Woo_Feed_Dropdown();
$product = new Woo_Feed_Products();
$product->load_attributes();

//echo "<pre>";
//print_r($product->getProductMeta(146,"_regular_price_EUR"));
//die();

?>
<div class="wrap" id="Feed">
    <h2><?php echo _e('WooCommerce Product Feed', 'woo-feed'); ?></h2>
    <?php echo WPFFWMessage()->infoMessage1(); ?>
    <form action="" id="generateFeed" class="generateFeed" method="post">
        <table class="widefat fixed">
            <thead>
            <tr>
                <td colspan="2"><b><?php echo _e('Content Settings', 'woo-feed'); ?></b></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td width="30%"><b><?php echo _e('Template', 'woo-feed'); ?> <span class="requiredIn">*</span></b></td>
                <td>
                    <select wftitle="Select a template" name="provider" id="provider"
                            class="generalInput wfmasterTooltip" required>
                        <?php echo $dropDown->merchantsDropdown(); ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><b><?php echo _e('File Name', 'woo-feed'); ?> <span class="requiredIn">*</span></b></td>
                <td><input wftitle="Filename should be unique. Otherwise it will override the existing filename."
                           name="filename" type="text" class="generalInput wfmasterTooltip" required="required"/>
                </td>
            </tr>
            <tr>
                <td><b><?php echo _e('Feed Type', 'woo-feed'); ?> <span class="requiredIn">*</span></b></td>
                <td>
                    <select name="feedType" id="feedType" class="generalInput" required>
                        <option value=""></option>
                        <option value="xml">XML</option>
                        <option value="csv">CSV</option>
                        <option value="txt">TXT</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td id="WFisVariations"><b><?php echo _e('Remove Out Of Stock Products?', 'woo-feed'); ?> <span class="requiredIn">*</span></b></td>
                <td>
                    <input type="radio"  name="is_outOfStock" id="" class="" value="y" >Yes
                    <input type="radio" name="is_outOfStock" id="" class="" value="n" checked>No
                </td>
            </tr>
            <tr>
                <td id="WFisVariations"><b><?php echo _e('Include Variations?', 'woo-feed'); ?> <span class="requiredIn">*</span></b></td>
                <td>
                    <input type="radio"  name="is_variations" id="radio_y" class="WFisVariations" value="y" checked>Yes
                    <input type="radio" name="is_variations" id="radio_n" class="WFisVariations" value="n">No
                </td>
            </tr>
            <tr class="WFVariablePriceTR">
                <td><b><?php echo _e('Variable Product Price', 'woo-feed'); ?></b></td>
                <td>
                    <input type="radio" name="variable_price" class="WFVariablePrice" value="max">Max Variation Price
                    <input type="radio" name="variable_price" class="WFVariablePrice" value="min" checked>Min Variation Price
                </td>
            </tr>
            <tr>
                <td><b><?php echo _e('Variable Product Quantity', 'woo-feed'); ?></b></td>
                <td>
                    <input type="radio" name="variable_quantity" class="WFVariableQty" value="first" checked> First Variation Quantity
                    <input type="radio" name="variable_quantity" class="WFVariableQty" value="max">Max Variation Quantity
                    <input type="radio" name="variable_quantity" class="WFVariableQty" value="min">Min Variation Quantity
                    <input type="radio" name="variable_quantity" class="WFVariableQty" value="sum">Sum of Variation Quantities 
                </td>
            </tr>
            <?php
            if (class_exists('SitePress')) {
                $my_current_lang = apply_filters('wpml_current_language', NULL);
                $langs = $dropDown->getActiveLanguages($my_current_lang);
                if (!empty($langs['array'])) {
                    ?>
                    <tr>
                        <td><b><?php echo _e('Language', 'woo-feed'); ?> <span class="requiredIn">*</span></b></td>
                        <td>
                            <select name="feedLanguage" id="feedType" class="generalInput" required>
                                <?php
                                echo $langs['ddown'];
                                ?>
                            </select>
                        </td>
                    </tr>
                <?php
                }
            }
            ?>
            <tr class="itemWrapper" style="display: none;">
                <td><b><?php echo _e('Items Wrapper', 'woo-feed'); ?> <span class="requiredIn">*</span></b></td>
                <td><input name="itemsWrapper" type="text" value="products" class="generalInput" required="required"/>
                </td>
            </tr>
            <tr class="itemWrapper" style="display: none;">
                <td><b><?php echo _e('Single Item Wrapper', 'woo-feed'); ?> <span class="requiredIn">*</span></b></td>
                <td><input name="itemWrapper" type="text" value="product" class="generalInput" required="required"/>
                </td>
            </tr>
            <tr class="itemWrapper" >
                <td><b><?php echo _e('Extra Header', 'woo-feed'); ?> </b></td>
                <td>
                    <textarea name="extraHeader" id="" style="width: 100%" placeholder="Insert Extra Header value. Press enter at the end of each line." rows="3"></textarea>
                </td>
            </tr>
            <tr class="wf_csvtxt" style="display: none;">
                <td><b><?php echo _e('Delimiter', 'woo-feed'); ?> <span class="requiredIn">*</span></b></td>
                <td>
                    <select name="delimiter" id="delimiter" class="generalInput">
                        <option value=",">Comma</option>
                        <option value="tab">Tab</option>
                        <option value=":">Colon</option>
                        <option value=" ">Space</option>
                        <option value="|">Pipe</option>
                        <option value=";">Semi Colon</option>
                    </select>
                </td>
            </tr>
            <tr class="wf_csvtxt" style="display: none;">
                <td><b><?php echo _e('Enclosure', 'woo-feed'); ?> <span class="requiredIn">*</span></b></td>
                <td>
                    <select name="enclosure" id="enclosure" class="generalInput">
                        <option value='double'>"</option>
                        <option value="single">'</option>
                        <option value=" ">None</option>
                    </select>
                </td>
            </tr>
            
            <tr>
                <td><b><?php echo _e('Extended Product Title', 'woo-feed'); ?></b></td>
                <td>
                    <select name="wpf_ptitle" id="wpf_ptitle"  class="generalInput">
                        <?php echo $product->attributeDropdown(); ?>
                    </select>
                    <br/>
                    <textarea name="ptitle_show" id="" cols="29" rows="4"></textarea>
                </td>
            </tr>
            <tr>
                <td><b>Categories</b></td>
                <td>
                    <select name="categories[]" class="wf_catsss wf_categories generalInput" multiple>
                        <?php
                        $categories = $dropDown->categories();
                        if (count($categories)) {
                            $str = "";
                            foreach ($categories as $key => $value) {
                                $str .= "<option value=$key>$value</option>";
                            }
                            echo $str;
                        }
                        ?>
                    </select>
                    <span style="font-size: x-small"><i>Keep blank to select all categories</i></span>
                </td>
            </tr>
            </tbody>
        </table>
        <br/><br/>

        <div id="providerPage">

        </div>
    </form>


</div><!-- /wrap -->

