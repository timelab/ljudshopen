<li>
	<input type="radio" name="wf_tabs" id="tab3"/>
	<label class="wf-tab-name" for="tab3"><?php echo _e( 'FTP', 'woo-feed' ); ?></label>

	<div id="wf-tab-content3" class="wf-tab-content">
		<table class="table widefat fixed mtable" width="100%">
			<tbody>
			<tr>
				<td><?php echo _e( 'Enabled', 'woo-feed' ); ?></td>
				<td>
					<select name="ftpenabled" id="">
						<option value="0"><?php echo _e( 'Disabled', 'woo-feed' ); ?></option>
						<option value="1"><?php echo _e( 'Enabled', 'woo-feed' ); ?></option>
					</select>
				</td>
			</tr>
			<tr>
				<td><?php echo _e( 'Host Name', 'woo-feed' ); ?></td>
				<td><input type="text" name="ftphost"/></td>
			</tr>

			<tr>
				<td><?php echo _e( 'Port', 'woo-feed' ); ?></td>
				<td><input type="text" name="ftpport" value="21"/></td>
			</tr>
			<tr>
				<td><?php echo _e( 'User Name', 'woo-feed' ); ?></td>
				<td><input type="text" autocomplete="nope" name="ftpuser"/></td>
			</tr>
			<tr>
				<td><?php echo _e( 'Password', 'woo-feed' ); ?></td>
				<td><input type="password" autocomplete="new-password" name="ftppassword"/></td>
			</tr>
			<tr>
				<td><?php echo _e( 'Path', 'woo-feed' ); ?></td>
				<td><input type="text" name="ftppath"/></td>
			</tr>
			</tbody>
		</table>
		<table class=" widefat fixed">
			<tr>
				<td align="left" class="makeFeedResponse">

				</td>
				<td align="right">
					<button type="submit" id="wf_submit" class="wfbtn">
						<?php echo _e( 'Save & Generate Feed', 'woo-feed' ); ?>
					</button>
				</td>
			</tr>
		</table>
	</div>
</li>