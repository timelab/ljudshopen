
<?php

$str=<<<EOT
<?xml version="1.0" encoding="utf-8" ?>
<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">
  <channel>
    <title>Data feed Title</title>
    <link><![CDATA[http://www.yoursiteurl.com]]></link>
    <description>Data feed description.</description>
    {each product start}
      <item>
        <g:id><![CDATA[{id}]]></g:id>
        <title><![CDATA[{title}]]></title>
        <link><![CDATA[{link}]]></link>
        <g:price>{(return \$price / 2;)} USD</g:price>
        <description><![CDATA[{description,[strip_tags]}]]></description>
        <g:product_type><![CDATA[{product_type}]]></g:product_type>
        <g:google_product_category><![CDATA[]]></g:google_product_category>
        <g:image_link>{image}</g:image_link>
        <g:condition>new</g:condition>
        <g:availability>in stock</g:availability>>
        <g:brand><![CDATA[{manufacturer}]]></g:brand>
        <g:mpn><![CDATA[{sku}]]></g:mpn>
      </item>
    {each product end}
  </channel>
</rss>
EOT;
//
?>
<ul class="wf_tabs">
<li>
    <input type="radio" name="wf_tabs" id="tab1" checked/>
    <label class="wf-tab-name" for="tab1"><?php echo _e('Feed Config', 'woo-feed'); ?></label>

    <div id="wf-tab-content1" class="wf-tab-content">
        <table class=" widefat fixed">
            <tr>
                <td colspan="3">
                    <textarea name="feed_config_custom2" id="editor" style="width: 100%;font-weight: bold;" rows="20"><?php echo $str;?></textarea>
                </td>
            </tr>
            <tr>
                <td align="left" class="">
                    <label for="feed_config_custom2_attribute">Product Attributes</label>
                </td>
                <td align="left">
                    <select id="feed_config_custom2_attribute"
                            class="wf_validate_attr wf_attr wf_attributes">
                        <?php echo $product->attributeDropdown(); ?>
                    </select>
                </td>
                <td>
                   <b><span id="feed_config_custom2_attribute_value"></span></b>
                </td>
            </tr>
        </table>
        <table class=" widefat fixed">
            <tr>
                <td align="left" class="">
                    <div class="makeFeedResponse"></div>
                    <div class="makeFeedComplete"></div>
                </td>
                <td align="right">
                    <button type="submit" id="wf_submit" class="wfbtn">
                        <?php echo _e('Save & Generate Feed', 'woo-feed'); ?>
                    </button>
                </td>
            </tr>
        </table>
    </div>
</li>
<?php if (get_option('woocommerce_product_feed_pro_activated') && get_option('woocommerce_product_feed_pro_activated') == "Activated") { ?>
    <li>
        <input type="radio" name="wf_tabs" id="tab2"/>
        <label class="wf-tab-name" for="tab2"><?php echo _e('Filter', 'woo-feed'); ?></label>

        <div id="wf-tab-content2" class="wf-tab-content">
            <table class="table tree widefat fixed sorted_table mtable" width="100%" id="table-filter">
                <thead>
                <tr>
                    <th></th>
                    <th><?php echo _e('Attributes', 'woo-feed'); ?></th>
                    <th><?php echo _e('Condition', 'woo-feed'); ?></th>
                    <th><?php echo _e('Value', 'woo-feed'); ?></th>
                    <th></th>
                </tr>
                <tr style="border-bottom: 2px solid #ccc">
                    <td><?php echo _e('Filter', 'woo-feed'); ?> </td>
                    <td colspan="4">
                        <select name="filterType" class="">
                            <option value="2"><?php echo _e('Together', 'woo-feed'); ?></option>
                            <option value="1"><?php echo _e('Individually', 'woo-feed'); ?></option>
                        </select>
                    </td>
                </tr>
                </thead>
                <tbody>

                <tr style="display:none;" class="daRow">
                    <td>
                        <i class="wf_sortedtable dashicons dashicons-menu"></i>
                    </td>
                    <td>
                        <select name="fattribute[]" id="" disabled required class="fsrow">
                            <?php echo $product->attributeDropdown(); ?>
                        </select>
                    </td>
                    <td>
                        <select name="condition[]" disabled class="fsrow">
                            <option value="=="><?php echo _e('is / equal', 'woo-feed'); ?></option>
                            <option value="!="><?php echo _e('is not / not equal', 'woo-feed'); ?></option>
                            <option value=">="><?php echo _e('equals or greater than', 'woo-feed'); ?></option>
                            <option value=">"><?php echo _e('greater than', 'woo-feed'); ?></option>
                            <option value="<="><?php echo _e('equals or less than', 'woo-feed'); ?></option>
                            <option value="<"><?php echo _e('less than', 'woo-feed'); ?></option>
                            <option value="contains"><?php echo _e('contains', 'woo-feed'); ?></option>
                            <option value="nContains"><?php echo _e('does not contain', 'woo-feed'); ?></option>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="filterCompare[]" disabled autocomplete="off" class="fsrow"/>
                    </td>
                    <td>
                        <i class="delRow dashicons dashicons-trash"></i>
                    </td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td>
                        <button type="button" class="button-small button-primary" id="wf_newFilter">
                            <?php echo _e('Add New Condition', 'woo-feed'); ?>
                        </button>
                    </td>
                    <td colspan="4">

                    </td>
                </tr>
                </tfoot>
            </table>
            <table class=" widefat fixed">
                <tr>
                    <td align="left" class="makeFeedResponse">

                    </td>
                    <td align="right">
                        <button type="submit" id="wf_submit" class="wfbtn">
                            <?php echo _e('Save & Generate Feed', 'woo-feed'); ?>
                        </button>
                    </td>
                </tr>
            </table>
        </div>
    </li>
<?php } ?>
<li>
    <input type="radio" name="wf_tabs" id="tab3"/>
    <label class="wf-tab-name" for="tab3"><?php echo _e('FTP', 'woo-feed'); ?></label>

    <div id="wf-tab-content3" class="wf-tab-content">
        <table class="table widefat fixed mtable" width="100%">
            <tbody>
            <tr>
                <td><?php echo _e('Enabled', 'woo-feed'); ?></td>
                <td>
                    <select name="ftpenabled" id="">
                        <option value="0"><?php echo _e('Disabled', 'woo-feed'); ?></option>
                        <option value="1"><?php echo _e('Enabled', 'woo-feed'); ?></option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><?php echo _e('Host Name', 'woo-feed'); ?></td>
                <td><input type="text" name="ftphost"/></td>
            </tr>
            <tr>
                <td><?php echo _e('User Name', 'woo-feed'); ?></td>
                <td><input type="text" name="ftpuser"/></td>
            </tr>
            <tr>
                <td><?php echo _e('Password', 'woo-feed'); ?></td>
                <td><input type="password" name="ftppassword"/></td>
            </tr>
            <tr>
                <td><?php echo _e('Path', 'woo-feed'); ?></td>
                <td><input type="text" name="ftppath"/></td>
            </tr>
            </tbody>
        </table>
        <table class=" widefat fixed">
            <tr>
                <td align="left" class="makeFeedResponse">

                </td>
                <td align="right">
                    <button type="submit" id="wf_submit" class="wfbtn">
                        <?php echo _e('Save & Generate Feed', 'woo-feed'); ?>
                    </button>
                </td>
            </tr>
        </table>
    </div>
</li>

</ul>
