<?php if (get_option('woocommerce_product_feed_pro_activated') && get_option('woocommerce_product_feed_pro_activated') == "Activated") { ?>
	<li>
		<input type="radio" name="wf_tabs" id="tab2"/>
		<label class="wf-tab-name" for="tab2"><?php echo _e( 'Filter', 'woo-feed' ); ?></label>

		<div id="wf-tab-content2" class="wf-tab-content">
			<table class="table tree widefat fixed sorted_table mtable" width="100%" id="table-filter">
				<thead>
				<tr>
					<th></th>
					<th><?php echo _e( 'Attributes', 'woo-feed' ); ?></th>
					<th><?php echo _e( 'Condition', 'woo-feed' ); ?></th>
					<th><?php echo _e( 'Value', 'woo-feed' ); ?></th>
					<th></th>
				</tr>
				<tr style="border-bottom: 2px solid #ccc">
					<td><?php echo _e( 'Filter', 'woo-feed' ); ?> </td>
					<td colspan="4">
						<select name="filterType" class="">
							<option value="2"><?php echo _e( 'Together', 'woo-feed' ); ?></option>
							<option value="1"><?php echo _e( 'Individually', 'woo-feed' ); ?></option>
						</select>
					</td>
				</tr>
				</thead>
				<tbody>

				<tr style="display:none;" class="daRow">
					<td>
						<i class="wf_sortedtable dashicons dashicons-menu"></i>
					</td>
					<td>
						<select name="fattribute[]" id="" disabled required class="fsrow">
							<?php echo $product->attributeDropdown(); ?>
						</select>
					</td>
					<td>
						<select name="condition[]" disabled class="fsrow">
							<option value="=="><?php echo _e( 'is / equal', 'woo-feed' ); ?></option>
							<option value="!="><?php echo _e( 'is not / not equal', 'woo-feed' ); ?></option>
							<option value=">="><?php echo _e( 'equals or greater than', 'woo-feed' ); ?></option>
							<option value=">"><?php echo _e( 'greater than', 'woo-feed' ); ?></option>
							<option value="<="><?php echo _e( 'equals or less than', 'woo-feed' ); ?></option>
							<option value="<"><?php echo _e( 'less than', 'woo-feed' ); ?></option>
							<option value="contains"><?php echo _e( 'contains', 'woo-feed' ); ?></option>
							<option value="nContains"><?php echo _e( 'does not contain', 'woo-feed' ); ?></option>
						</select>
					</td>
					<td>
						<input type="text" name="filterCompare[]" disabled autocomplete="off" class="fsrow"/>
					</td>
					<td>
						<i class="delRow dashicons dashicons-trash"></i>
					</td>
				</tr>
				</tbody>
				<tfoot>
				<tr>
					<td>
						<button type="button" class="button-small button-primary" id="wf_newFilter">
							<?php echo _e( 'Add New Condition', 'woo-feed' ); ?>
						</button>
					</td>
					<td colspan="4">

					</td>
				</tr>
				</tfoot>
			</table>
			<table class=" widefat fixed">
				<tr>
					<td align="left" class="makeFeedResponse">

					</td>
					<td align="right">
						<button type="submit" id="wf_submit" class="wfbtn">
							<?php echo _e( 'Save & Generate Feed', 'woo-feed' ); ?>
						</button>
					</td>
				</tr>
			</table>
		</div>
	</li>
<?php } ?>