<?php

/**
 * Class Custom
 *
 * Responsible for processing and generating custom feed
 *
 * @since 1.0.0
 * @package Shopping
 *
 */
class Woo_Feed_Custom2
{
    /**
     * This variable is responsible for holding all product attributes and their values
     *
     * @since   1.0.0
     * @var     array $products Contains all the product attributes to generate feed
     * @access  public
     */
    public $products;

    /**
     * This variable is responsible for holding feed configuration form values
     *
     * @since   1.0.0
     * @var     Custom $rules Contains feed configuration form values
     * @access  public
     */
    public $rules;

    /**
     * This variable is responsible for holding feed string
     *
     * @since   1.0.0
     * @var     Custom $feedString Contains feed information
     * @access  public
     */
    public $feedHeader;
    public $feedString;
    public $feedFooter;
    private $engine;

    public $s=-1;
    /**
     * Define the core functionality to generate feed.
     *
     * Set the feed rules. Map products according to the rules and Check required attributes
     * and their values according to merchant specification.
     * @var Woo_Generate_Feed $feedRule Contain Feed Configuration
     * @since    1.0.0
     */
    public function __construct($feedRule)
    {

        $products = new Woo_Feed_Products();
        
        $this->feedHeader="";
        $this->feedString="";
        $this->feedFooter="";
        
        $limit =isset($feedRule['Limit'])?esc_html($feedRule['Limit']):"";
        $offset = isset($feedRule['Offset'])?esc_html($feedRule['Offset']):"";
        $categories = isset($feedRule['categories']) ? $feedRule['categories']: "";
        $this->products = $products->woo_feed_get_visible_product($limit,$offset,$categories,$feedRule);
        $feedRule=$products->feedRule;
        $this->engine = new WF_Engine($this->products,$feedRule);
        $this->rules = $feedRule;
        $this->process_xml($feedRule['feed_config_custom2'],$feedRule);
    }



    public function make_xml_header($xml){
        $getHeader=explode('{each product start}',$xml);
        $header=$getHeader[0];
        $getNodes = explode("\n",$header);
        if(!empty($getNodes)){
            foreach($getNodes as $key=>$value){
                #Add header info to feed file
                $value=preg_replace('/\\\\/','',$value);
                $this->feedHeader.=$value;
                $this->s=$this->s+2;
            }
        }else{
            $this->feedHeader.='<?xml version="1.0" encoding="utf-8" ?>'."\n";
        }
    }

    public function save_xml(){
        $upload_dir = wp_upload_dir();
        $base = $upload_dir['basedir'];
        $fileName="feed.xml";
        # Check If any products founds
        if (!empty($this->feedString)) {
            # Save File
            $path = $base . "/woo-feed";
            $file = $path . "/" . $fileName;

            $save = new Woo_Feed_Savefile();
            $save->saveFile($path, $file, $this->feedString);
        }else{
            return false;
        }
    }

    public function make_xml_footer($xml){
        $getFooter=explode('{each product end}',$xml);
        $getNodes = explode("\n", $getFooter[1]);
        if(!empty($getNodes)){
            foreach($getNodes as $key=>$value){
                $this->s=$this->s-2;
                #Add header info to feed file
                $this->feedFooter.=$value;
            }
        }
    }



    public function process_xml($xml,$rule)
    {
        $this->make_xml_header($xml);
        $XMLNodes=$this->getXMLNodes($xml);

        $p=false;
        if(!empty($XMLNodes) && !empty($this->products)){
            foreach($this->products as $key=>$product){
                $feedString="";
                # Start making XML node
                foreach($XMLNodes as $each=>$attribute){
                    $cdata=$this->getCDATA(isset($attribute['cdata'])?$attribute['cdata']:"");
                    $start="";$end="";$output="";
                    # Get the parent node
                    if(empty($attribute['attr']) && empty($attribute['end']) && count($attribute)==3){
                        #Start XML Node
                        $end.="<".$attribute['start'].">";
                        $feedString.=$end."\n";
                        $this->s=$this->s+2;
                        $p=true;
                    }else if(!empty($attribute['start'])){
                        $nodeStart=$value=preg_replace('/\\\\/','',$attribute['start']);
                        $start.="<".$nodeStart.">";
                    }

                    #Make XML Node Value
                    if(!empty($attribute['attr'])){
                        if($attribute['attr_type']=="attribute"){
                            if(array_key_exists($attribute['attr_code'],$this->products[$key])){
                                if($attribute['attr_code']=="title"){
                                    $title=$this->products[$key][$attribute['attr_code']];
                                    $output=$this->get_formatted_variation_title($attribute,$product,$title);
                                }else{
                                    $output=$this->products[$key][$attribute['attr_code']];
                                }
                            }else{
                                $output=$this->getAttributeTypeAndValue($attribute['attr_code'],$this->products[$key]);
                                $this->products[$key][$attribute['attr_code']]=$output;
                            }
                        }elseif($attribute['attr_type']=="return"){
                            $output=$this->getReturnTypeValue($attribute,$this->products[$key],$key);
                        }else if($attribute['attr_type']=="php"){
                            if(isset($attribute['to_return']) && !empty($attribute['to_return'])){
                                $output=$this->returnPHPFunction($attribute['to_return']);
                            }
                        }elseif($attribute['attr_type']=="text"){
                            $output=(isset($attribute['attr_value']) && !empty($attribute['attr_value']))?$attribute['attr_value']:"";
                        }
                    }

                    if($product['variation_type']="child" && array_key_exists('id_type',$attribute) && !empty($attribute['id_type'])){
                        $type=$attribute['id_type'];
                        $parent=$this->searchProductInArray($this->products,"id",$this->products[$key]['item_group_id']);
                        if(!empty($parent)){
                            $parent=array_reduce($parent, 'array_merge', array());
                        }

                        if($type=="parent"){ // Will set parent attribute value if parent is not empty
                            if(!empty($parent) && isset($parent[$attribute['attr_code']]) && !empty($parent[$attribute['attr_code']])){
                                $output=$parent[$attribute['attr_code']];
                            }
                        }elseif($type=="only_parent"){ // Will only set parent attribute value whatever the value is empty or not
                            if(!empty($parent) && isset($parent[$attribute['attr_code']])) {
                                $output = $parent[$attribute['attr_code']];
                            }
                        }elseif($type=="parent_if_empty"){ // Will set parent attribute value if child is empty
                            if(empty($this->products[$key][$attribute['attr_code']])){
                                if(!empty($parent) && !empty($parent[$attribute['attr_code']])){
                                    $output=$parent[$attribute['attr_code']];
                                }
                            }
                        }
                    }

                    if(array_key_exists('formatter',$attribute)){
                        $output=$this->outputFormatter($output,$attribute,$this->products[$key]);
                    }


                    if($attribute['start']=="/".$attribute['end'] && empty($attribute['attr']) && count($attribute)==3){
                        #End XML Node
                        if(!empty($attribute['end'])){
                            $end.="<".$attribute['start'].">";
                        }
                        $p=true;
                        $feedString.=$end."\n";
                    }else{
                        if(!empty($attribute['end'])){
                            $end.="</".$attribute['end'].">";
                        }
                    }
                    if(!$p){
                        $prefix="";$suffix="";
                        if(!empty($attribute['prefix'])){
                            $prefix=$attribute['prefix']." ";
                            $prefix=preg_replace('!\s+!', ' ', $prefix);
                        }
                        if(!empty($attribute['suffix'])){
                            # No space if url
                            if(substr($output,0,4)=="http"){
                                $suffix=$attribute['suffix'];
                            }else{
                                $suffix=" ".$attribute['suffix'];
                                $suffix=preg_replace('!\s+!', ' ', $suffix);
                            }

                        }
                        if(!empty($output)){
                            $node=$start.$cdata[0].$prefix.$output.$suffix.$cdata[1].$end;
                        }else{
                            $node=$start.$output.$end;
                        }
                        $feedString.=$node."\n";
                        $this->products[$key][$attribute['attr_code']]=$output;
                    }
                    $p=false;
                }
                $engine=new WF_Engine($this->products,$this->rules);
                # Filter products by condition
                if (isset($this->rules['fattribute']) && count($this->rules['fattribute'])) {
                    if (!$engine->filter_product($key, $product)) {
                        $feedString="";
                        continue;
                    }else{
                        $this->feedString.=$feedString;
                    }
                }else{
                    $this->feedString.=$feedString;
                }
            }
        }

        $this->make_xml_footer($xml);
        return $this->feedString;
    }


    public function searchProductInArray($products, $key, $value)
    {
        $results = array();
        if (is_array($products)) {
            if (isset($products[$key]) && $products[$key] == $value) {
                $results[] = $products;
            }

            foreach ($products as $subarray) {
                $results = array_merge($results, $this->searchProductInArray($subarray, $key, $value));
            }
        }
        return $results;
    }



    public function getReturnTypeValue($attribute,$product,$pid){
        if (strpos($attribute['to_return'], '$') !== false) {
            $pattern = '/\$\S+/';
            preg_match_all($pattern, $attribute['attr'], $matches, PREG_SET_ORDER);
            $matches = array_column($matches, 0);
            foreach ($matches as $key => $value) {
                $value = str_replace("$", "", $value);
                if (!array_key_exists(trim($value), $product)) {
                    $attribute['attr_code'] = $value;
                    $product[$value] = $this->getAttributeTypeAndValue($attribute['attr_code'], $product);
                    $this->products[$pid][$attribute['attr_code']]=$product[$value];
                }
            }
        }

        extract($product);
        $return=$attribute['to_return'];
        $return=preg_replace('/\\\\/','',$return);
        return eval($return);
    }


    public function outputFormatter($output,$attribute,$product){
        if(!empty($attribute['formatter'])){
            $output=trim($output);
            $engine=new WF_Engine($this->products,$this->rules);
            foreach($attribute['formatter'] as $key=>$value){

                if(!empty($value) && !empty($output)){

                    if(strpos($value, 'substr') !== false){
                        $args=preg_split('/\s+/', $value);
                        $output=substr($output,$args[1],$args[2]);
                    }else if(strpos($value, 'strip_tags') !== false){
                        $output= strip_tags(html_entity_decode($output));
                    }else if(strpos($value, 'htmlentities') !== false){
                        $output= htmlentities($output);
                    }else if(strpos($value, 'clear') !== false){
                        $output=$engine->stripInvalidXml($output);
                    }else if(strpos($value, 'ucwords') !== false){
                        $output = ucwords(strtolower($output));
                    }else if(strpos($value, 'ucfirst') !== false){
                        $output = ucfirst(strtolower($output));
                    }else if(strpos($value, 'strtoupper') !== false){
                        $output= strtoupper(strtolower($output));
                    }else if(strpos($value, 'strtolower') !== false){
                        $output= strtolower($output);
                    }else if(strpos($value, 'convert') !== false){
                        $args=preg_split('/\s+/', $value);
                        $output=$engine->convertCurrency($output,$args[1],$args[2]);
                    }else if(strpos($value, 'number_format') !== false){
                        //$args=preg_split('/\s+/', $value);
                        $args=explode(" ",$value,3);
                        if(isset($args[1]) && isset($args[2])){
                            $output= number_format($output,$args[1],$args[2]);
                        }else if(isset($args[1])){
                            $output= number_format($output,$args[1]);
                        }else{
                            $output= number_format($output);
                        }
                    }else if(strpos(strtolower($value), 'urltounsecure') !== false){
                        if(substr($output,0,4)=="http")
                            $output= str_replace("https://","http://",$output);
                    }else if(strpos(strtolower($value), 'urltosecure') !== false){
                        if(substr($output,0,4)=="http")
                            $output= str_replace("http://","https://",$output);
                    }else if(strpos($value, 'str_replace') !== false){
                        $args=explode(">",$value,3);
                        if(array_key_exists(1,$args) && array_key_exists(2,$args)){
                            $param1=!empty($args[1])?trim($args[1]):" ";
                            $param2=!empty($args[2])?trim($args[2]):" ";
                            $output=str_replace($param1,$param2,$output);
                        }
                    }else if(strpos($value, 'strip_shortcodes') !== false){
	                    $productClass = new Woo_Feed_Products();
	                    $output=$productClass->remove_short_codes($output);
                    }
                }
            }
            return $output;
        }
        return false;
    }

    public function getAttributeTypeAndValue($attrCode,$product){
        $engine = new WF_Engine($this->products,$this->rules);
        if(array_key_exists($attrCode,$product)){
            return $product[$attrCode];
        }else if (strpos($attrCode, 'wf_cmapping_') !== false) {
            return $engine->get_category_mapping_value($attrCode,$product['item_group_id']);
        }else if (strpos($attrCode, 'wf_option_') !== false) {
            return get_option(str_replace('wf_option_', '', $attrCode));
        }else if (strpos($attrCode, 'wf_dattribute_') !== false) {
            return $engine->get_dynamic_attribute_value($attrCode, $product);
        }else if (strpos($attrCode, 'wf_attr_') !== false) {
            return implode(',', wc_get_product_terms($product['id'], str_replace("wf_attr_", "", $attrCode), array('fields' => 'names')));
        }else if (strpos($attrCode, 'wf_cattr_') !== false) {
            return $engine->get_custom_attribute_value($attrCode,$product['id'],$product['item_group_id']);
        }else if (strpos($attrCode, 'wf_taxo_') !== false) {
            $productClass=new Woo_Feed_Products();
            return $productClass->get_product_term_list($product['id'], str_replace("wf_taxo_", "", $attrCode));
        }else{
            return "";
        }
    }

    /** Return the php function of the attribute
     * @param $function
     * @return mixed
     */
    public function returnPHPFunction($function){
        // $function = eval("return $function");
        return $function;
    }

    /** Get XML node information for each product
     * @param $xml
     * @return array
     */
    public function getXMLNodes($xml)
    {
        $xml=trim(preg_replace('/\+/', '', $xml));
        # Get XML nodes for each products
        $getFeedBody = $this->engine->get_string_between($xml,'{each product start}', '{each product end}');
        # Explode each node by new line
        $getNodes = explode("\n", $getFeedBody);
        $XMLNodes=array();
        $i=1;
        if(!empty($getNodes)){
            foreach($getNodes as $key=>$value){
                if(!empty($value)){
                    # Check Node
                    $node = $this->engine->get_string_between($value, '<', '>');
                    if(empty($node)){
                        continue;
                    }
                    # Get node starting
                    $XMLNodes[$i]['start'] = $node;
                    # Get node ending
                    $XMLNodes[$i]['end'] = $this->engine->get_string_between($value,"</",">");
                    # Set CDATA status and remove CDATA
                    $attr=$this->engine->get_string_between($value, '>', '</');
                    if (strpos($attr, 'CDATA') !== false) {
                        $XMLNodes[$i]['cdata']="yes";
                        $attr=str_replace(array("<![CDATA[","]]>"),array('',''),$attr);
                    }
                    # Get Pattern of the xml node
                    $XMLNodes[$i]['attr'] = $attr;

                    if(!empty($XMLNodes[$i]['attr'])){
                        # Get type of the attribute pattern
                        if(strpos($attr,"{")===false and strpos($attr,"}")===false){
                            $XMLNodes[$i]['attr_type']="text";
                            $XMLNodes[$i]['attr_value']=$attr;
                        }elseif(strpos($attr, 'return') !== false){
                            $XMLNodes[$i]['attr_type']="return";
                            $return=$this->engine->get_string_between($attr,"{(",")}");
                            $XMLNodes[$i]['to_return']=$return;
                        }elseif(strpos($attr, 'php ') !== false){
                            $XMLNodes[$i]['attr_type']="php";
                            $php=$this->engine->get_string_between($attr,"{(",")}");
                            $XMLNodes[$i]['to_return']=str_replace("php","",$php);
                        }else{
                            $XMLNodes[$i]['attr_type']="attribute";
                            $attribute=$this->engine->get_string_between($attr,"{","}");
                            $getAttrBaseFormat=explode(",",$attribute);
                            $attrInfo=$getAttrBaseFormat[0];
                            if(count($getAttrBaseFormat)>1){
                                $j=0;
                                foreach($getAttrBaseFormat as $key=>$value){
                                    if(!empty($value)){
                                        $XMLNodes[$i]['formatter'][$j]= $this->engine->get_string_between($value, '[', ']');
                                        $j++;
                                    }
                                }
                            }

                            $getAttrCodes=explode("|",$attrInfo);
                            $XMLNodes[$i]['attr_code']=$getAttrCodes[0];
                            $XMLNodes[$i]['id_type']=isset($getAttrCodes[1])?$getAttrCodes[1]:"";
                        }

                        # Get prefix of the attribute node value
                        $XMLNodes[$i]['prefix']="";
                        if(substr(trim($attr),0,1)!="{" &&  $XMLNodes[$i]['attr_type']!="text"){
                            $getPrefix=explode("{",$attr);
                            $XMLNodes[$i]['prefix']=(count($getPrefix)>1)?$getPrefix[0]:"";
                        }
                        # Get suffix of the attribute node value
                        $XMLNodes[$i]['suffix']="";
                        if(substr(trim($attr),0,1)!="}" &&  $XMLNodes[$i]['attr_type']!="text"){
                            $getSuffix=explode("}",$attr);
                            $XMLNodes[$i]['suffix']=(count($getSuffix)>1)?$getSuffix[1]:"";
                        }
                    }
                    $i++;
                }
            }
        }
        return $XMLNodes;
    }


    /** Get CDATA status of a node
     * @param string $status
     * @return array
     */
    public function getCDATA($status=""){
        if($status=="yes"){
            return array("<![CDATA[","]]>");
        }else{
            return array("","");
        }
    }

    /** Get formatted variation product title
     * @param $attribute
     * @param $product
     * @param string $title
     * @return mixed|string
     */
    public function get_formatted_variation_title($attribute,$product,$title = "")
    {
        $getTitle=isset($this->rules['ptitle_show'])&& !empty($this->rules['ptitle_show'])?explode("|",$this->rules['ptitle_show']):"";

        if(!empty($getTitle) && count($getTitle)){
            $str="";
            if(!in_array('title',$getTitle)){
                $str.= $title." ";
            }
            foreach($getTitle as $key=>$attr){
                if(!empty($attr)){
                    $value=$this->getAttributeTypeAndValue($attr,$product);
                    $str.= " ".$value;
                }
            }
            return preg_replace('!\s+!', ' ', $str);
        }else{
            return $title;
        }
    }

    /**
     * Return Feed
     *
     * @return array|bool|string
     */
    public function returnFinalProduct()
    {
        $feed=array(
            "header"=>$this->feedHeader,
            "body"=>$this->feedString,
            "footer"=>$this->feedFooter,
        );
        return $feed;
    }
}

if (!function_exists('array_column')) {
    /**
     * Returns the values from a single column of the input array, identified by
     * the $columnKey.
     *
     * Optionally, you may provide an $indexKey to index the values in the returned
     * array by the values from the $indexKey column in the input array.
     *
     * @param array $input A multi-dimensional array (record set) from which to pull
     *                     a column of values.
     * @param mixed $columnKey The column of values to return. This value may be the
     *                         integer key of the column you wish to retrieve, or it
     *                         may be the string key name for an associative array.
     * @param mixed $indexKey (Optional.) The column to use as the index/keys for
     *                        the returned array. This value may be the integer key
     *                        of the column, or it may be the string key name.
     * @return array
     */
    function array_column($input = null, $columnKey = null, $indexKey = null)
    {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();

        if ($argc < 2) {
            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
            return null;
        }

        if (!is_array($params[0])) {
            trigger_error(
                'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
                E_USER_WARNING
            );
            return null;
        }

        if (!is_int($params[1])
            && !is_float($params[1])
            && !is_string($params[1])
            && $params[1] !== null
            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
        ) {
            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        if (isset($params[2])
            && !is_int($params[2])
            && !is_float($params[2])
            && !is_string($params[2])
            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
        ) {
            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

        $paramsIndexKey = null;
        if (isset($params[2])) {
            if (is_float($params[2]) || is_int($params[2])) {
                $paramsIndexKey = (int) $params[2];
            } else {
                $paramsIndexKey = (string) $params[2];
            }
        }

        $resultArray = array();

        foreach ($paramsInput as $row) {
            $key = $value = null;
            $keySet = $valueSet = false;

            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                $keySet = true;
                $key = (string) $row[$paramsIndexKey];
            }

            if ($paramsColumnKey === null) {
                $valueSet = true;
                $value = $row;
            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }

            if ($valueSet) {
                if ($keySet) {
                    $resultArray[$key] = $value;
                } else {
                    $resultArray[] = $value;
                }
            }

        }

        return $resultArray;
    }

}