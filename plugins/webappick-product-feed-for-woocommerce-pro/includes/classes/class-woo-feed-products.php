<?php

/**
 * This is used to store all the information about wooCommerce store products
 *
 * @since      1.0.0
 * @package    Woo_Feed
 * @subpackage Woo_Feed/includes
 * @author     Ohidul Islam <wahid@webappick.com>
 */
class Woo_Feed_Products
{

	/**
	 * Contain all parent product information for the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $parent Contain all parent product information for the plugin.
	 */
	public $parent;

	/**
	 * Contain all child product information for the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $parent Contain all child product information for the plugin.
	 */
	public $child;

	/**
	 * The parent id of current product.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $parentID The current product's Parent ID.
	 */
	public $parentID;
	/**
	 * The child id of current product.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $parentID The current product's child ID.
	 */
	public $childID;

	/**
	 * The Variable that contain all products.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array $productsList Products list array.
	 */
	public $productsList;

	/**
	 * Hold Modified Filter Condition
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array filter condition array.
	 */
	public $feedRule;
	public $ids_in=array();
	public $ids_not_in=array();
	public $categories;
	public $WPFP_WPML=false;
	public $currentLang=false;
	public $pi;


	public function wooProductQuery($arg) {

		global $wpdb;


		$categories="";
		if(isset($arg['category']) && $arg['category']!=""){
			$catIds=array();

			foreach ($arg['category'] as $key=>$value){
				$catid=get_term_by('slug',$value,'product_cat');
				array_push($catIds,$catid->term_id);
			}

			$categories=implode(",",$catIds);
			$categories="AND {$wpdb->prefix}term_taxonomy.term_id IN ($categories)";
		}

		$variations="";
		if(isset($this->feedRule['is_variations']) && $this->feedRule['is_variations']!="n"){
			$variations="OR {$wpdb->prefix}posts.post_type = 'product_variation'";
		}

		$limit="";
		$offset="";
		if($arg['limit']!="" && $arg['limit']!="-1" && $arg['limit']>0){
			$limit=$arg['limit'];
			$limit="LIMIT $limit";


			if($arg['offset']!="" && $arg['offset']!="-1"){
				$offset=$arg['offset'];
				$offset="OFFSET $offset";
			}
		}



		$stock="";
		if(isset($arg['stock_status']) && $arg['stock_status']=="instock"){
			$stock="AND {$wpdb->prefix}postmeta.meta_value='instock'";
		}

		$idsNotIn="";
		if(isset($arg['exclude']) && $arg['exclude']!=""){
			$idsNotIn=is_array($arg['exclude'])?implode(",",$arg['exclude']):$arg['exclude'];
			$idsNotIn="AND {$wpdb->prefix}posts.ID NOT IN ($idsNotIn)";
		}

		$idsIn="";
		if(isset($arg['include']) && $arg['include']!=""){
			$idsIn=is_array($arg['include'])?implode(",",$arg['include']):$arg['include'];
			$idsIn="AND {$wpdb->prefix}posts.ID IN ($idsIn)";
		}

		$query = "SELECT DISTINCT {$wpdb->prefix}posts.ID 
				FROM {$wpdb->prefix}posts
				LEFT JOIN {$wpdb->prefix}term_relationships ON ({$wpdb->prefix}posts.ID = {$wpdb->prefix}term_relationships.object_id) 
				LEFT JOIN {$wpdb->prefix}term_taxonomy ON ({$wpdb->prefix}term_relationships.term_taxonomy_id = {$wpdb->prefix}term_taxonomy.term_taxonomy_id)
				LEFT JOIN {$wpdb->prefix}postmeta ON ({$wpdb->prefix}posts.ID = {$wpdb->prefix}postmeta.post_id) 
				WHERE {$wpdb->prefix}posts.post_status = 'publish' $stock $categories $idsNotIn $idsIn AND ({$wpdb->prefix}posts.post_type = 'product' $variations)		  
				ORDER BY ID DESC $limit $offset";
		$products = $wpdb->get_results($query, ARRAY_A );
		return $products;
	}


	public function getWC3Products($limit, $offset) {

		$limit  = ! empty( $limit ) && is_numeric( $limit ) ? absint( $limit ) : '-1';
		$offset = ! empty( $offset ) && is_numeric( $offset ) ? absint( $offset ) : '0';

		# Query Arguments
		$arg = array(
			'limit'   => $limit,
			'offset'  => $offset,
			'status'  => 'publish',
			'orderby' => 'date',
			'order'   => 'DESC',
			'return' => 'ids',
		);


		if ( get_option( 'woocommerce_product_feed_pro_activated' ) && get_option( 'woocommerce_product_feed_pro_activated' ) == "Activated" ) {

			# Remove products by id
			$this->inExProducts();
			if ( sizeof( $this->ids_not_in ) > 0 ) {
				$arg['exclude'] = $this->ids_not_in;
			}

			if ( sizeof( $this->ids_in ) > 0 ) {
				$arg['include'] = $this->ids_in;
			}

			# Remove Out Of Stock Products
			if ( isset( $this->feedRule['is_outOfStock'] ) && $this->feedRule['is_outOfStock'] == "y" ) {
				$arg['stock_status'] = "instock";
			}

			# Get Products for Specific Categories
			if ( is_array( $this->categories ) && ! empty( $this->categories[0] ) ) {
				$arg['category'] = $this->categories;
			}

		}

		# Don't Include Variations for Facebook
		if ( $this->feedRule['provider'] == 'facebook' ) {
			$this->feedRule['is_variations'] = 'n';
		}

		$types=array('simple','variable','grouped','external','composite','bundle');



		# Product Type
		$arg['type'] = $types;

		$query    = new WC_Product_Query( $arg );
		$products = $query->get_products();

		# Debug
		#return $products;

		$this->pi = 1; # Product Index
		foreach ( $products as $key => $productId ) {
			$prod=wc_get_product($productId);

			if(!$prod && !is_object($prod)){
				continue;
			}

			$id     = $prod->get_id();

			if ( $prod->is_type( 'simple' ) )
			{

				$simple = new WC_Product_Simple( $id );

				if($simple->get_status()!="publish"){
					continue;
				}

				$this->productsList[ $this->pi ]["id"]             = $simple->get_id();
				$this->productsList[ $this->pi ]["title"]          = $simple->get_name();
				$this->productsList[ $this->pi ]["description"]    = $this->remove_short_codes($simple->get_description());
				$this->productsList[ $this->pi ]['variation_type'] = $simple->get_type();

				$this->productsList[ $this->pi ]['short_description'] = $this->remove_short_codes($simple->get_short_description());
				$this->productsList[ $this->pi ]['product_type']      = strip_tags( wc_get_product_category_list( $id, ">", "" ) );
				$this->productsList[ $this->pi ]['link']              = $simple->get_permalink();
				$this->productsList[ $this->pi ]['ex_link']           = $simple->get_permalink();


				# Featured Image
				if ( has_post_thumbnail( $id ) ):
					$image                                            = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' );
					$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( $image[0] );
					$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( $image[0] );
				else:
					$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( wp_get_attachment_url( $id ) );
					$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( wp_get_attachment_url( $id ) );
				endif;

				# Additional Images
				$attachmentIds = $simple->get_gallery_image_ids();
				$imgUrls       = array();
				if ( $attachmentIds && is_array( $attachmentIds ) ) {
					$mKey = 1;
					foreach ( $attachmentIds as $attachmentId ) {
						$imgUrls[ $mKey ]                               = $this->get_formatted_url( wp_get_attachment_url( $attachmentId ) );
						$this->productsList[ $this->pi ]["image_$mKey"] = $imgUrls[ $mKey ];
						$mKey ++;
					}

				}

				$this->productsList[ $this->pi ]['images']           = implode( ",", $imgUrls );
				$this->productsList[ $this->pi ]['condition']        = "New";
				$this->productsList[ $this->pi ]['type']             = $simple->get_type();
				$this->productsList[ $this->pi ]['is_bundle']        = "no";
				$this->productsList[ $this->pi ]['multipack']        = "";
				$this->productsList[ $this->pi ]['visibility']       = $simple->get_catalog_visibility();
				$this->productsList[ $this->pi ]['rating_total']     = $simple->get_rating_count();
				$this->productsList[ $this->pi ]['rating_average']   = $simple->get_average_rating();
				$this->productsList[ $this->pi ]['tags']             = strip_tags( wc_get_product_tag_list( $id, "," ) );
				$this->productsList[ $this->pi ]['item_group_id']    = $simple->get_id();
				$this->productsList[ $this->pi ]['sku']              = $simple->get_sku();
				$this->productsList[ $this->pi ]['availability']     = $this->availability( $id );
				$this->productsList[ $this->pi ]['quantity']         = $simple->get_stock_quantity();
				$this->productsList[ $this->pi ]['sale_price_sdate'] = $simple->get_date_on_sale_from();
				$this->productsList[ $this->pi ]['sale_price_edate'] = $simple->get_date_on_sale_to();
				$this->productsList[ $this->pi ]['price']            = $simple->get_regular_price();
				$this->productsList[ $this->pi ]['current_price']    = $simple->get_price();
				$this->productsList[ $this->pi ]['price_with_tax']   = ( $simple->is_taxable() ) ? $this->getPriceWithTax( $simple ) : $simple->get_price();
				$this->productsList[ $this->pi ]['sale_price']       = $simple->get_sale_price();
				$this->productsList[ $this->pi ]['weight']           = $simple->get_weight();
				$this->productsList[ $this->pi ]['width']            = $simple->get_width();
				$this->productsList[ $this->pi ]['height']           = $simple->get_height();
				$this->productsList[ $this->pi ]['length']           = $simple->get_length();
				$this->productsList[ $this->pi ]['shipping_class']   = $simple->get_shipping_class();


				# Sale price effective date
				$from = $this->sale_price_effective_date( $id, '_sale_price_dates_from' );
				$to   = $this->sale_price_effective_date( $id, '_sale_price_dates_to' );
				if ( ! empty( $from ) && ! empty( $to ) ) {
					$from                                                         = date( "c", strtotime( $from ) );
					$to                                                           = date( "c", strtotime( $to ) );
					$this->productsList[ $this->pi ]['sale_price_effective_date'] = "$from" . "/" . "$to";
				} else {
					$this->productsList[ $this->pi ]['sale_price_effective_date'] = "";
				}

			}
			else if ( $prod->is_type( 'external' ) ) {

				$external = new WC_Product_External( $id );

				if($external->get_status()!="publish"){
					continue;
				}

				$this->productsList[ $this->pi ]["id"]             = $external->get_id();
				$this->productsList[ $this->pi ]["title"]          = $external->get_name();
				$this->productsList[ $this->pi ]["description"]    = $this->remove_short_codes($external->get_description());
				$this->productsList[ $this->pi ]['variation_type'] = $external->get_type();

				$this->productsList[ $this->pi ]['short_description'] = $this->remove_short_codes($external->get_short_description());
				$this->productsList[ $this->pi ]['product_type']      = strip_tags( wc_get_product_category_list( $id, ">", "" ) );
				$this->productsList[ $this->pi ]['link']              = $external->get_permalink();
				$this->productsList[ $this->pi ]['ex_link']           = $external->get_product_url();


				# Featured Image
				if ( has_post_thumbnail( $id ) ):
					$image                                            = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' );
					$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( $image[0] );
					$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( $image[0] );
				else:
					$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( wp_get_attachment_url( $id ) );
					$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( wp_get_attachment_url( $id ) );
				endif;

				# Additional Images
				$attachmentIds = $external->get_gallery_image_ids();
				$imgUrls       = array();
				if ( $attachmentIds && is_array( $attachmentIds ) ) {
					$mKey = 1;
					foreach ( $attachmentIds as $attachmentId ) {
						$imgUrls[ $mKey ]                               = $this->get_formatted_url( wp_get_attachment_url( $attachmentId ) );
						$this->productsList[ $this->pi ]["image_$mKey"] = $imgUrls[ $mKey ];
						$mKey ++;
					}

				}

				$this->productsList[ $this->pi ]['images']           = implode( ",", $imgUrls );
				$this->productsList[ $this->pi ]['condition']        = "New";
				$this->productsList[ $this->pi ]['type']             = $external->get_type();
				$this->productsList[ $this->pi ]['is_bundle']        = "no";
				$this->productsList[ $this->pi ]['multipack']        = "";
				$this->productsList[ $this->pi ]['visibility']       = $external->get_catalog_visibility();
				$this->productsList[ $this->pi ]['rating_total']     = $external->get_rating_count();
				$this->productsList[ $this->pi ]['rating_average']   = $external->get_average_rating();
				$this->productsList[ $this->pi ]['tags']             = strip_tags( wc_get_product_tag_list( $id, "," ) );
				$this->productsList[ $this->pi ]['item_group_id']    = $external->get_id();
				$this->productsList[ $this->pi ]['sku']              = $external->get_sku();
				$this->productsList[ $this->pi ]['availability']     = $this->availability( $id );
				$this->productsList[ $this->pi ]['quantity']         = $external->get_stock_quantity();
				$this->productsList[ $this->pi ]['sale_price_sdate'] = $external->get_date_on_sale_from();
				$this->productsList[ $this->pi ]['sale_price_edate'] = $external->get_date_on_sale_to();
				$this->productsList[ $this->pi ]['price']            = $external->get_regular_price();
				$this->productsList[ $this->pi ]['current_price']    = $external->get_price();
				$this->productsList[ $this->pi ]['price_with_tax']   = ( $external->is_taxable() ) ? $this->getPriceWithTax( $external ) : $external->get_price();
				$this->productsList[ $this->pi ]['sale_price']       = $external->get_sale_price();
				$this->productsList[ $this->pi ]['weight']           = $external->get_weight();
				$this->productsList[ $this->pi ]['width']            = $external->get_width();
				$this->productsList[ $this->pi ]['height']           = $external->get_height();
				$this->productsList[ $this->pi ]['length']           = $external->get_length();
				$this->productsList[ $this->pi ]['shipping_class']   = $external->get_shipping_class();


				# Sale price effective date
				$from = $this->sale_price_effective_date( $id, '_sale_price_dates_from' );
				$to   = $this->sale_price_effective_date( $id, '_sale_price_dates_to' );
				if ( ! empty( $from ) && ! empty( $to ) ) {
					$from                                                         = date( "c", strtotime( $from ) );
					$to                                                           = date( "c", strtotime( $to ) );
					$this->productsList[ $this->pi ]['sale_price_effective_date'] = "$from" . "/" . "$to";
				} else {
					$this->productsList[ $this->pi ]['sale_price_effective_date'] = "";
				}
			}
			else if ( $prod->is_type( 'grouped' ) ) {

				$grouped = new WC_Product_Grouped( $id );

				if($grouped->get_status()!="publish"){
					continue;
				}

				$this->productsList[ $this->pi ]["id"]             = $grouped->get_id();
				$this->productsList[ $this->pi ]["title"]          = $grouped->get_name();
				$this->productsList[ $this->pi ]["description"]    = $this->remove_short_codes($grouped->get_description());
				$this->productsList[ $this->pi ]['variation_type'] = $grouped->get_type();

				$this->productsList[ $this->pi ]['short_description'] = $this->remove_short_codes($grouped->get_short_description());
				$this->productsList[ $this->pi ]['product_type']      = strip_tags( wc_get_product_category_list( $id, ">", "" ) );
				$this->productsList[ $this->pi ]['link']              = $grouped->get_permalink();
				$this->productsList[ $this->pi ]['ex_link']           = $grouped->get_permalink();


				# Featured Image
				if ( has_post_thumbnail( $id ) ):
					$image                                            = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' );
					$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( $image[0] );
					$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( $image[0] );
				else:
					$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( wp_get_attachment_url( $id ) );
					$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( wp_get_attachment_url( $id ) );
				endif;

				# Additional Images
				$attachmentIds = $grouped->get_gallery_image_ids();
				$imgUrls       = array();
				if ( $attachmentIds && is_array( $attachmentIds ) ) {
					$mKey = 1;
					foreach ( $attachmentIds as $attachmentId ) {
						$imgUrls[ $mKey ]                               = $this->get_formatted_url( wp_get_attachment_url( $attachmentId ) );
						$this->productsList[ $this->pi ]["image_$mKey"] = $imgUrls[ $mKey ];
						$mKey ++;
					}

				}

				$this->productsList[ $this->pi ]['images']           = implode( ",", $imgUrls );
				$this->productsList[ $this->pi ]['condition']        = "New";
				$this->productsList[ $this->pi ]['type']             = $grouped->get_type();
				$this->productsList[ $this->pi ]['is_bundle']        = "no";
				$this->productsList[ $this->pi ]['multipack']        = ( count( $grouped->get_children() ) > 0 ) ? count( $grouped->get_children() ) : "";
				$this->productsList[ $this->pi ]['visibility']       = $grouped->get_catalog_visibility();
				$this->productsList[ $this->pi ]['rating_total']     = $grouped->get_rating_count();
				$this->productsList[ $this->pi ]['rating_average']   = $grouped->get_average_rating();
				$this->productsList[ $this->pi ]['tags']             = strip_tags( wc_get_product_tag_list( $id, "," ) );
				$this->productsList[ $this->pi ]['item_group_id']    = $grouped->get_id();
				$this->productsList[ $this->pi ]['sku']              = $grouped->get_sku();
				$this->productsList[ $this->pi ]['availability']     = $this->availability( $id );
				$this->productsList[ $this->pi ]['quantity']         = $grouped->get_stock_quantity();
				$this->productsList[ $this->pi ]['sale_price_sdate'] = $grouped->get_date_on_sale_from();
				$this->productsList[ $this->pi ]['sale_price_edate'] = $grouped->get_date_on_sale_to();
				$this->productsList[ $this->pi ]['price']            = $grouped->get_regular_price();
				$this->productsList[ $this->pi ]['current_price']    = $grouped->get_price();
				$this->productsList[ $this->pi ]['price_with_tax']   = ( $grouped->is_taxable() ) ? $this->getPriceWithTax( $grouped ) : $grouped->get_price();
				$this->productsList[ $this->pi ]['sale_price']       = $grouped->get_sale_price();
				$this->productsList[ $this->pi ]['weight']           = $grouped->get_weight();
				$this->productsList[ $this->pi ]['width']            = $grouped->get_width();
				$this->productsList[ $this->pi ]['height']           = $grouped->get_height();
				$this->productsList[ $this->pi ]['length']           = $grouped->get_length();
				$this->productsList[ $this->pi ]['shipping_class']   = $grouped->get_shipping_class();


				# Sale price effective date
				$from = $this->sale_price_effective_date( $id, '_sale_price_dates_from' );
				$to   = $this->sale_price_effective_date( $id, '_sale_price_dates_to' );
				if ( ! empty( $from ) && ! empty( $to ) ) {
					$from                                                         = date( "c", strtotime( $from ) );
					$to                                                           = date( "c", strtotime( $to ) );
					$this->productsList[ $this->pi ]['sale_price_effective_date'] = "$from" . "/" . "$to";
				} else {
					$this->productsList[ $this->pi ]['sale_price_effective_date'] = "";
				}
			}
			else if ( $prod->is_type( 'composite' ) ) {
				if ( class_exists( "WC_Product_Composite" ) ) {

					$composite = new WC_Product_Composite( $id );

					if($composite->get_status()!="publish"){
						continue;
					}

					$this->productsList[ $this->pi ]["id"]             = $composite->get_id();
					$this->productsList[ $this->pi ]["title"]          = $composite->get_name();
					$this->productsList[ $this->pi ]["description"]    = $this->remove_short_codes($composite->get_description());
					$this->productsList[ $this->pi ]['variation_type'] = $composite->get_type();

					$this->productsList[ $this->pi ]['short_description'] = $this->remove_short_codes($composite->get_short_description());
					$this->productsList[ $this->pi ]['product_type']      = strip_tags( wc_get_product_category_list( $id, ">", "" ) );
					$this->productsList[ $this->pi ]['link']              = $composite->get_permalink();
					$this->productsList[ $this->pi ]['ex_link']           = $composite->get_permalink();


					# Featured Image
					if ( has_post_thumbnail( $id ) ):
						$image                                            = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' );
						$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( $image[0] );
						$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( $image[0] );
					else:
						$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( wp_get_attachment_url( $id ) );
						$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( wp_get_attachment_url( $id ) );
					endif;

					# Additional Images
					$attachmentIds = $composite->get_gallery_image_ids();
					$imgUrls       = array();
					if ( $attachmentIds && is_array( $attachmentIds ) ) {
						$mKey = 1;
						foreach ( $attachmentIds as $attachmentId ) {
							$imgUrls[ $mKey ]                               = $this->get_formatted_url( wp_get_attachment_url( $attachmentId ) );
							$this->productsList[ $this->pi ]["image_$mKey"] = $imgUrls[ $mKey ];
							$mKey ++;
						}
					}

					$this->productsList[ $this->pi ]['images']         = implode( ",", $imgUrls );
					$this->productsList[ $this->pi ]['condition']      = "New";
					$this->productsList[ $this->pi ]['type']           = $composite->get_type();
					$this->productsList[ $this->pi ]['is_bundle']      = "no";
					$this->productsList[ $this->pi ]['multipack']      = "";
					$this->productsList[ $this->pi ]['visibility']     = $composite->get_catalog_visibility();
					$this->productsList[ $this->pi ]['rating_total']   = $composite->get_rating_count();
					$this->productsList[ $this->pi ]['rating_average'] = $composite->get_average_rating();
					$this->productsList[ $this->pi ]['tags']           = strip_tags( wc_get_product_tag_list( $id, "," ) );
					$this->productsList[ $this->pi ]['item_group_id']  = $composite->get_id();
					$this->productsList[ $this->pi ]['sku']            = $composite->get_sku();
					$this->productsList[ $this->pi ]['availability']   = $this->availability( $id );

					if ( $this->feedRule['variable_price'] == 'first' ) {
						$price  = $composite->get_composite_regular_price();
						$sPrice = $composite->get_composite_price();
					} else {
						$price  = $composite->get_composite_regular_price( $this->feedRule['variable_price'] );
						$sPrice = $composite->get_composite_price( $this->feedRule['variable_price'] );
					}

					$this->productsList[ $this->pi ]['quantity']         = $composite->get_stock_quantity();
					$this->productsList[ $this->pi ]['sale_price_sdate'] = $composite->get_date_on_sale_from();
					$this->productsList[ $this->pi ]['sale_price_edate'] = $composite->get_date_on_sale_to();
					$this->productsList[ $this->pi ]['price']            = $price;
					$this->productsList[ $this->pi ]['current_price']    = $composite->get_price();
					$this->productsList[ $this->pi ]['price_with_tax']   = $composite->get_composite_price_including_tax();
					$this->productsList[ $this->pi ]['sale_price']       = $sPrice;
					$this->productsList[ $this->pi ]['weight']           = $composite->get_weight();
					$this->productsList[ $this->pi ]['width']            = $composite->get_width();
					$this->productsList[ $this->pi ]['height']           = $composite->get_height();
					$this->productsList[ $this->pi ]['length']           = $composite->get_length();
					$this->productsList[ $this->pi ]['shipping_class']   = $composite->get_shipping_class();


					# Sale price effective date
					$from = $this->sale_price_effective_date( $id, '_sale_price_dates_from' );
					$to   = $this->sale_price_effective_date( $id, '_sale_price_dates_to' );
					if ( ! empty( $from ) && ! empty( $to ) ) {
						$from                                                         = date( "c", strtotime( $from ) );
						$to                                                           = date( "c", strtotime( $to ) );
						$this->productsList[ $this->pi ]['sale_price_effective_date'] = "$from" . "/" . "$to";
					} else {
						$this->productsList[ $this->pi ]['sale_price_effective_date'] = "";
					}
				}

			}
			else if ( $prod->is_type( 'bundle' ) ) {
				if ( class_exists( "WC_Product_Bundle" ) ) {

					$bundle = new WC_Product_Bundle( $id );

					if($bundle->get_status()!="publish"){
						continue;
					}

					$this->productsList[ $this->pi ]["id"]             = $bundle->get_id();
					$this->productsList[ $this->pi ]["title"]          = $bundle->get_name();
					$this->productsList[ $this->pi ]["description"]    = $this->remove_short_codes($bundle->get_description());
					$this->productsList[ $this->pi ]['variation_type'] = $bundle->get_type();

					$this->productsList[ $this->pi ]['short_description'] = $this->remove_short_codes($bundle->get_short_description());
					$this->productsList[ $this->pi ]['product_type']      = strip_tags( wc_get_product_category_list( $id, ">", "" ) );
					$this->productsList[ $this->pi ]['link']              = $bundle->get_permalink();
					$this->productsList[ $this->pi ]['ex_link']           = $bundle->get_permalink();


					# Featured Image
					if ( has_post_thumbnail( $id ) ):
						$image                                            = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' );
						$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( $image[0] );
						$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( $image[0] );
					else:
						$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( wp_get_attachment_url( $id ) );
						$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( wp_get_attachment_url( $id ) );
					endif;

					# Additional Images
					$attachmentIds = $bundle->get_gallery_image_ids();
					$imgUrls       = array();
					if ( $attachmentIds && is_array( $attachmentIds ) ) {
						$mKey = 1;
						foreach ( $attachmentIds as $attachmentId ) {
							$imgUrls[ $mKey ]                               = $this->get_formatted_url( wp_get_attachment_url( $attachmentId ) );
							$this->productsList[ $this->pi ]["image_$mKey"] = $imgUrls[ $mKey ];
							$mKey ++;
						}

					}

					$this->productsList[ $this->pi ]['images']         = implode( ",", $imgUrls );
					$this->productsList[ $this->pi ]['condition']      = "New";
					$this->productsList[ $this->pi ]['type']           = $bundle->get_type();
					$this->productsList[ $this->pi ]['is_bundle']      = "yes";
					$this->productsList[ $this->pi ]['multipack']      = "";
					$this->productsList[ $this->pi ]['visibility']     = $bundle->get_catalog_visibility();
					$this->productsList[ $this->pi ]['rating_total']   = $bundle->get_rating_count();
					$this->productsList[ $this->pi ]['rating_average'] = $bundle->get_average_rating();
					$this->productsList[ $this->pi ]['tags']           = strip_tags( wc_get_product_tag_list( $id, "," ) );
					$this->productsList[ $this->pi ]['item_group_id']  = $bundle->get_id();
					$this->productsList[ $this->pi ]['sku']            = $bundle->get_sku();
					$this->productsList[ $this->pi ]['availability']   = $this->availability( $id );

					if ( $this->feedRule['variable_price'] == 'first' ) {
						$price  = $bundle->get_bundle_regular_price();
						$sPrice = $bundle->get_bundle_price();
					} else {
						$price  = $bundle->get_bundle_regular_price( $this->feedRule['variable_price'] );
						$sPrice = $bundle->get_bundle_price( $this->feedRule['variable_price'] );
					}

					$this->productsList[ $this->pi ]['quantity']         = $bundle->get_stock_quantity();
					$this->productsList[ $this->pi ]['sale_price_sdate'] = $bundle->get_date_on_sale_from();
					$this->productsList[ $this->pi ]['sale_price_edate'] = $bundle->get_date_on_sale_to();
					$this->productsList[ $this->pi ]['price']            = $price;
					$this->productsList[ $this->pi ]['current_price']    = $bundle->get_price();
					$this->productsList[ $this->pi ]['price_with_tax']   = $bundle->get_bundle_price_including_tax();
					$this->productsList[ $this->pi ]['sale_price']       = $sPrice;
					$this->productsList[ $this->pi ]['weight']           = $bundle->get_weight();
					$this->productsList[ $this->pi ]['width']            = $bundle->get_width();
					$this->productsList[ $this->pi ]['height']           = $bundle->get_height();
					$this->productsList[ $this->pi ]['length']           = $bundle->get_length();
					$this->productsList[ $this->pi ]['shipping_class']   = $bundle->get_shipping_class();


					# Sale price effective date
					$from = $this->sale_price_effective_date( $id, '_sale_price_dates_from' );
					$to   = $this->sale_price_effective_date( $id, '_sale_price_dates_to' );
					if ( ! empty( $from ) && ! empty( $to ) ) {
						$from                                                         = date( "c", strtotime( $from ) );
						$to                                                           = date( "c", strtotime( $to ) );
						$this->productsList[ $this->pi ]['sale_price_effective_date'] = "$from" . "/" . "$to";
					} else {
						$this->productsList[ $this->pi ]['sale_price_effective_date'] = "";
					}
				}

			}
			else if ( $prod->is_type( 'variable' )) { # If product variations are set to exclude then only include variable (parent) products

				$variable = new WC_Product_Variable( $id );

				if($this->feedRule['is_variations']!="y"){

					if($variable->get_status()!="publish"){
						continue;
					}

					$this->productsList[ $this->pi ]["id"]             = $variable->get_id();
					$this->productsList[ $this->pi ]["title"]          = $variable->get_name();
					$this->productsList[ $this->pi ]["description"]    = $this->remove_short_codes($variable->get_description());
					$this->productsList[ $this->pi ]['variation_type'] = $variable->get_type();

					$this->productsList[ $this->pi ]['short_description'] = $this->remove_short_codes($variable->get_short_description());
					$this->productsList[ $this->pi ]['product_type']      = strip_tags( wc_get_product_category_list( $id, ">", "" ) );
					$this->productsList[ $this->pi ]['link']              = $variable->get_permalink();
					$this->productsList[ $this->pi ]['ex_link']           = $variable->get_permalink();


					# Featured Image
					if ( has_post_thumbnail( $id ) ):
						$image                                            = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' );
						$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( $image[0] );
						$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( $image[0] );
					else:
						$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( wp_get_attachment_url( $id ) );
						$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( wp_get_attachment_url( $id ) );
					endif;

					# Additional Images
					$attachmentIds = $variable->get_gallery_image_ids();
					$imgUrls       = array();
					if ( $attachmentIds && is_array( $attachmentIds ) ) {
						$mKey = 1;
						foreach ( $attachmentIds as $attachmentId ) {
							$imgUrls[ $mKey ]                               = $this->get_formatted_url( wp_get_attachment_url( $attachmentId ) );
							$this->productsList[ $this->pi ]["image_$mKey"] = $imgUrls[ $mKey ];
							$mKey ++;
						}

					}

					$this->productsList[ $this->pi ]['images']         = implode( ",", $imgUrls );
					$this->productsList[ $this->pi ]['condition']      = "New";
					$this->productsList[ $this->pi ]['type']           = $variable->get_type();
					$this->productsList[ $this->pi ]['is_bundle']      = "no";
					$this->productsList[ $this->pi ]['multipack']      = "";
					$this->productsList[ $this->pi ]['visibility']     = $variable->get_catalog_visibility();
					$this->productsList[ $this->pi ]['rating_total']   = $variable->get_rating_count();
					$this->productsList[ $this->pi ]['rating_average'] = $variable->get_average_rating();
					$this->productsList[ $this->pi ]['tags']           = strip_tags( wc_get_product_tag_list( $id, "," ) );
					$this->productsList[ $this->pi ]['item_group_id']  = $variable->get_id();
					$this->productsList[ $this->pi ]['sku']            = $variable->get_sku();
					$this->productsList[ $this->pi ]['availability']   = $this->availability( $id );

					if( $this->feedRule['variable_price'] == 'min' ){
						$price  = $variable->get_variation_price('min', $variable->is_taxable() );
						$sPrice = $variable->get_variation_sale_price( $this->feedRule['variable_price'], $variable->is_taxable() );
					}else if( $this->feedRule['variable_price'] == 'max' ){
						$price  = $variable->get_variation_price('max', $variable->is_taxable() );
						$sPrice = $variable->get_variation_sale_price( $this->feedRule['variable_price'], $variable->is_taxable() );
					}else{
						$price  = $variable->get_variation_regular_price();
						$sPrice = $variable->get_variation_sale_price();
					}

					$stock=$this->get_quantity($id,"_stock");

					$this->productsList[ $this->pi ]['quantity']         = $stock;// $variable->get_stock_quantity();
					$this->productsList[ $this->pi ]['sale_price_sdate'] = $variable->get_date_on_sale_from();
					$this->productsList[ $this->pi ]['sale_price_edate'] = $variable->get_date_on_sale_to();
					$this->productsList[ $this->pi ]['price']            = $price;
					$this->productsList[ $this->pi ]['current_price']    = $variable->get_price();
					$this->productsList[ $this->pi ]['price_with_tax']   = ( $variable->is_taxable() ) ? $this->getPriceWithTax( $variable ) : $variable->get_price();
					$this->productsList[ $this->pi ]['sale_price']       = $sPrice;
					$this->productsList[ $this->pi ]['weight']           = $variable->get_weight();
					$this->productsList[ $this->pi ]['width']            = $variable->get_width();
					$this->productsList[ $this->pi ]['height']           = $variable->get_height();
					$this->productsList[ $this->pi ]['length']           = $variable->get_length();
					$this->productsList[ $this->pi ]['shipping_class']   = $variable->get_shipping_class();


					# Sale price effective date
					$from = $this->sale_price_effective_date( $id, '_sale_price_dates_from' );
					$to   = $this->sale_price_effective_date( $id, '_sale_price_dates_to' );
					if ( ! empty( $from ) && ! empty( $to ) ) {
						$from                                                         = date( "c", strtotime( $from ) );
						$to                                                           = date( "c", strtotime( $to ) );
						$this->productsList[ $this->pi ]['sale_price_effective_date'] = "$from" . "/" . "$to";
					} else {
						$this->productsList[ $this->pi ]['sale_price_effective_date'] = "";
					}
				}else{
					$this->getWC3Variations($variable->get_available_variations());
				}
			}

			$this->pi++;
		}

		return $this->productsList;

	}

	/**
	 * Get Product Variations of a variable product
	 * @since  2.2.0
	 * @for    WC 3.1+
	 * @param $variations
	 */
	public function getWC3Variations($variations) {

		if(is_array($variations) && (sizeof($variations) > 0 )){
			foreach ($variations as $vKey=>$variationProd){

				$this->pi++;
				$id=$variationProd['variation_id'];
				$variation=new WC_Product_Variation($id);

				$parent=new WC_Product_Variable($variation->get_parent_id());

				# Skip if not a valid product
				if(!$variation->variation_is_visible()){
					continue;
				}

				if($parent->get_status()!="publish"){
					continue;
				}

				# Get Variation Description
				$description=$variation->get_description();
				if(empty($description)){
					$description=$parent->get_description();
				}
				$description=$this->remove_short_codes($description);

				# Get Variation Short Description
				$short_description=$variation->get_short_description();
				if(empty($short_description)){
					$short_description=$parent->get_short_description();
				}
				$short_description=$this->remove_short_codes($short_description);

				$this->productsList[ $this->pi ]["id"]                = $variation->get_id();
				$this->productsList[ $this->pi ]["title"]             = $variation->get_name();
				$this->productsList[ $this->pi ]["parent_title"]      = $parent->get_name();
				$this->productsList[ $this->pi ]["description"]       = $description;
				$this->productsList[ $this->pi ]['variation_type']    = $variation->get_type();

				$this->productsList[ $this->pi ]['short_description'] = $short_description;
				$this->productsList[ $this->pi ]['product_type']      = strip_tags(wc_get_product_category_list($variation->get_parent_id(),">",""));
				$this->productsList[ $this->pi ]['link']              = $variation->get_permalink();
				$this->productsList[ $this->pi ]['ex_link']           = $variation->get_permalink();


				# Featured Image
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' );
				if ( has_post_thumbnail( $id ) && !empty($image[0])):

					$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( $image[0] );
					$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( $image[0] );
				else:
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $variation->get_parent_id() ), 'single-post-thumbnail' );
					$this->productsList[ $this->pi ]['image']         = $this->get_formatted_url( $image[0] );
					$this->productsList[ $this->pi ]['feature_image'] = $this->get_formatted_url( $image[0] );
				endif;

				# Additional Images
				$attachmentIds = $variation->get_gallery_image_ids();
				$imgUrls = array();
				if ( $attachmentIds && is_array( $attachmentIds ) ) {
					$mKey = 1;
					foreach ( $attachmentIds as $attachmentId ) {
						$imgUrls[$mKey] =$this->get_formatted_url( wp_get_attachment_url( $attachmentId ));
						$this->productsList[ $this->pi ]["image_$mKey"] = $imgUrls[$mKey];
						$mKey ++;
					}

				}

				$this->productsList[ $this->pi ]['images']         = implode( ",", $imgUrls );
				$this->productsList[ $this->pi ]['condition']      = "New";
				$this->productsList[ $this->pi ]['type']           = $variation->get_type();
				$this->productsList[ $this->pi ]['is_bundle']      = "no";
				$this->productsList[ $this->pi ]['multipack']      = "";
				$this->productsList[ $this->pi ]['visibility']     = $variation->get_catalog_visibility();
				$this->productsList[ $this->pi ]['rating_total']   = $variation->get_rating_count();
				$this->productsList[ $this->pi ]['rating_average'] = $variation->get_average_rating();
				$this->productsList[ $this->pi ]['tags']           = strip_tags(wc_get_product_tag_list($id,","));
				$this->productsList[ $this->pi ]['item_group_id']  = $variation->get_parent_id();
				$this->productsList[ $this->pi ]['sku']            = $variation->get_sku();
				$this->productsList[ $this->pi ]['availability']   = $this->availability( $id );
				$this->productsList[ $this->pi ]['quantity']         = $variation->get_stock_quantity();
				$this->productsList[ $this->pi ]['sale_price_sdate'] = $variation->get_date_on_sale_from();
				$this->productsList[ $this->pi ]['sale_price_edate'] = $variation->get_date_on_sale_to();
				$this->productsList[ $this->pi ]['price']            = $variation->get_regular_price();
				$this->productsList[ $this->pi ]['current_price']    = $variation->get_price();
				$this->productsList[ $this->pi ]['price_with_tax']   = ( $variation->is_taxable() ) ? $this->getPriceWithTax( $variation ) : $variation->get_price();
				$this->productsList[ $this->pi ]['sale_price']       = $variation->get_sale_price();
				$this->productsList[ $this->pi ]['weight']           = $variation->get_weight();
				$this->productsList[ $this->pi ]['width']            = $variation->get_width();
				$this->productsList[ $this->pi ]['height']           = $variation->get_height();
				$this->productsList[ $this->pi ]['length']           = $variation->get_length();
				$this->productsList[ $this->pi ]['shipping_class']   = $variation->get_shipping_class();


				# Sale price effective date
				$from = $this->sale_price_effective_date( $id, '_sale_price_dates_from' );
				$to   = $this->sale_price_effective_date( $id, '_sale_price_dates_to' );
				if ( ! empty( $from ) && ! empty( $to ) ) {
					$from                                                  = date( "c", strtotime( $from ) );
					$to                                                    = date( "c", strtotime( $to ) );
					$this->productsList[ $this->pi ]['sale_price_effective_date'] = "$from" . "/" . "$to";
				} else {
					$this->productsList[ $this->pi ]['sale_price_effective_date'] = "";
				}

			}
		}
	}

	/**
	 * Remove Short Codes from String
	 *
	 * @param $content
	 *
	 * @return mixed|string
	 */
	public function remove_short_codes($content)
	{
		if(empty($content)){
			return "";
		}

		# Remove Visual Composer Short Codes
		if(class_exists("WPBMap")){
			WPBMap::addAllMappedShortcodes(); // This does all the work
			$content = apply_filters( 'the_content',$content);
		}

		# Remove DIVI Builder Short Codes
		if(class_exists('ET_Builder_Module')){
			$content = preg_replace('/\[\/?et_pb.*?\]/', '', $content);
		}

		return do_shortcode($content);
	}

	/**
	 *  Get Ids to Exclude or Include from product list
	 *
	 * @since 2.2.0
	 * @for WC 3.1+
	 */
	public function inExProducts(  ) {
		# Argument for Product search by ID
		if ( isset( $this->feedRule['fattribute'] ) && is_array( $this->feedRule['fattribute'] ) ) {
			if ( count( $this->feedRule['fattribute'] ) ) {
				$condition        = $this->feedRule['condition'];
				$compare          = $this->feedRule['filterCompare'];
				$this->ids_in     = array();
				$this->ids_not_in = array();
				foreach ( $this->feedRule['fattribute'] as $key => $rule ) {
					if ( $rule == 'id' && in_array( $condition[ $key ], array( "==", "contain" ) ) ) {
						unset( $this->feedRule['fattribute'][ $key ] );
						unset( $this->feedRule['condition'][ $key ] );
						unset( $this->feedRule['filterCompare'][ $key ] );
						if ( strpos( $compare[ $key ], ',' ) !== false ) {
							foreach ( explode( ",", $compare[ $key ] ) as $key => $id ) {
								array_push( $this->ids_in, $id );
							}
						} else {
							array_push( $this->ids_in, $compare[ $key ] );
						}
					} elseif ( $rule == 'id' && in_array( $condition[ $key ], array( "!=", "nContains" ) ) ) {
						unset( $this->feedRule['fattribute'][ $key ] );
						unset( $this->feedRule['condition'][ $key ] );
						unset( $this->feedRule['filterCompare'][ $key ] );
						if ( strpos( $compare[ $key ], ',' ) !== false ) {
							foreach ( explode( ",", $compare[ $key ] ) as $key => $id ) {
								array_push( $this->ids_not_in, $id );
							}
						} else {
							array_push( $this->ids_not_in, $compare[ $key ] );
						}
					}
				}
			}
		}
	}


	/**
	 * Get WooCommerce Products
	 *
	 * @param $limit
	 * @param $offset
	 * @param $categories
	 * @param $feedRule
	 * @return array
	 */
	public function woo_feed_get_visible_product($limit, $offset,$categories,$feedRule) {

		$this->feedRule = $feedRule;
		$this->categories=$categories;

		if ( get_option( "WPFP_WPML" ) && get_option( "WPFP_WPML" ) == "yes" ) {
			$this->WPFP_WPML = true;
		}

		# WC 3.2+ Compatibility

		if ( woo_feed_wc_version_check(3.2) ) {

			$products = $this->getWC3Products( $limit, $offset);



			return $products;
		} else {

			try {

				if ( $this->WPFP_WPML ) {
					$this->currentLang = apply_filters( 'wpml_current_language', null );
					if ( isset( $feedRule['feedLanguage'] ) && $feedRule['feedLanguage'] != $this->currentLang ) {
						do_action( 'wpml_switch_language', $feedRule['feedLanguage'] );
					}
				}

				$limit  = ! empty( $limit ) && is_numeric( $limit ) ? absint( $limit ) : '-1';
				$offset = ! empty( $offset ) && is_numeric( $offset ) ? absint( $offset ) : '0';
				if ( get_option( 'woocommerce_product_feed_pro_activated' ) && get_option( 'woocommerce_product_feed_pro_activated' ) != "Activated" ) {
					$limit = 2000;
				}
				$arg = array(
					'post_type'              => array( 'product' ),
					'post_status'            => 'publish',
					'ignore_sticky_posts'    => 1,
					'posts_per_page'         => $limit,
					'orderby'                => 'date',
					'order'                  => 'desc',
					'fields'                 => 'ids',
					'offset'                 => $offset,
					'cache_results'          => false,
					'update_post_term_cache' => false,
					'update_post_meta_cache' => false,
				);


				if ( get_option( 'woocommerce_product_feed_pro_activated' ) && get_option( 'woocommerce_product_feed_pro_activated' ) == "Activated" ) {

					# Argument for Product search by ID
					if ( isset( $this->feedRule['fattribute'] ) && is_array( $this->feedRule['fattribute'] ) ) {
						if ( count( $this->feedRule['fattribute'] ) ) {
							$condition        = $this->feedRule['condition'];
							$compare          = $this->feedRule['filterCompare'];
							$this->ids_in     = array();
							$this->ids_not_in = array();
							foreach ( $this->feedRule['fattribute'] as $key => $rule ) {
								if ( $rule == 'id' && in_array( $condition[ $key ], array( "==", "contain" ) ) ) {
									unset( $this->feedRule['fattribute'][ $key ] );
									unset( $this->feedRule['condition'][ $key ] );
									unset( $this->feedRule['filterCompare'][ $key ] );
									if ( strpos( $compare[ $key ], ',' ) !== false ) {
										foreach ( explode( ",", $compare[ $key ] ) as $key => $id ) {
											array_push( $this->ids_in, $id );
										}
									} else {
										array_push( $this->ids_in, $compare[ $key ] );
									}
								} elseif ( $rule == 'id' && in_array( $condition[ $key ], array( "!=", "nContains" ) ) ) {
									unset( $this->feedRule['fattribute'][ $key ] );
									unset( $this->feedRule['condition'][ $key ] );
									unset( $this->feedRule['filterCompare'][ $key ] );
									if ( strpos( $compare[ $key ], ',' ) !== false ) {
										foreach ( explode( ",", $compare[ $key ] ) as $key => $id ) {
											array_push( $this->ids_not_in, $id );
										}
									} else {
										array_push( $this->ids_not_in, $compare[ $key ] );
									}
								}
							}

							if ( count( $this->ids_in ) ) {
								$arg['post__in'] = $this->ids_in;
							}

							if ( count( $this->ids_not_in ) ) {
								$arg['post__not_in'] = $this->ids_not_in;
							}
						}
					}

					if ( is_array( $categories ) && ! empty( $categories[0] ) ) {
						$i                            = 0;
						$arg['tax_query']['relation'] = "OR";
						foreach ( $categories as $key => $value ) {
							if ( ! empty( $value ) ) {
								$arg['tax_query'][ $i ]["taxonomy"] = "product_cat";
								$arg['tax_query'][ $i ]["field"]    = "slug";
								$arg['tax_query'][ $i ]["terms"]    = $value;
								$i ++;
							}
						}
					}
				}

				# Query Database for products
				$loop = new WP_Query( $arg );
				$i    = 0;

				while ( $loop->have_posts() ) : $loop->the_post();

					$this->childID  = get_the_ID();
					$this->parentID = wp_get_post_parent_id( $this->childID );

					global $product;
					$type1 = "";

					if ( ! is_object( $product ) ) {
						$product = wc_get_product( get_the_ID() );
						if ( ! is_object( $product ) ) {
							continue;
						}
					}

					if ( ! $product->is_visible() ) {
						continue;
					}

					if ( is_object( $product ) && $product->is_type( 'simple' ) ) {
						# No variations to product
						$type1 = "simple";
					} elseif ( is_object( $product ) && $product->is_type( 'variable' ) ) {
						# Product has variations
						$type1 = "variable";
					} elseif ( is_object( $product ) && $product->is_type( 'grouped' ) ) {
						$type1 = "grouped";
					} elseif ( is_object( $product ) && $product->is_type( 'external' ) ) {
						$type1 = "external";
					} elseif ( is_object( $product ) && $product->is_downloadable() ) {
						$type1 = "downloadable";
					} elseif ( is_object( $product ) && $product->is_virtual() ) {
						$type1 = "virtual";
					}

					$post = get_post( $this->parentID );


					if ( $post->post_status == 'trash' ) {
						continue;
					}

					if ( $this->feedRule['provider'] == 'facebook' ) {
						$this->feedRule['is_variations'] = 'n';
					}
					if ( ! isset( $this->feedRule['is_variations'] ) ) {
						$this->feedRule['is_variations'] = 'y';
					}

					if ( ! isset( $this->feedRule['is_outOfStock'] ) ) {
						$this->feedRule['is_outOfStock'] = 'n';
					}

					if ( ! isset( $this->feedRule['variable_price'] ) ) {
						$this->feedRule['variable_price'] = 'first';
					}

					if ( get_post_type() == 'product' ) {
						if ( $type1 == 'simple' ) {

							if ( $this->feedRule['is_outOfStock'] == 'y' ) {
								if ( $this->availability( $post->ID ) == "out of stock" ) {
									continue;
								}
							}

							$mainImage = wp_get_attachment_url( $product->get_image_id() );
							$link      = get_permalink( $post->ID );


							if ( $this->feedRule['provider'] != 'custom' ) {
								if ( substr( trim( $link ), 0, 4 ) !== "http" && substr( trim( $mainImage ), 0, 4 ) !== "http" ) {
									continue;
								}
							}

							$this->productsList[ $i ]['id']             = $post->ID;
							$this->productsList[ $i ]['variation_type'] = "simple";
							$this->productsList[ $i ]['title']          = $post->post_title;
							$this->productsList[ $i ]['description']    =  $post->post_content;

							$this->productsList[ $i ]['short_description'] = $post->post_excerpt;
							$this->productsList[ $i ]['product_type']      = $this->get_product_term_list( $post->ID, 'product_cat', "", ">" );// $this->categories($this->parentID);//TODO
							$this->productsList[ $i ]['link']              = $link;
							$this->productsList[ $i ]['ex_link']           = "";
							$this->productsList[ $i ]['image']             = $this->get_formatted_url( $mainImage );

							# Featured Image
							if ( has_post_thumbnail( $post->ID ) ):
								$image                                     = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
								$this->productsList[ $i ]['feature_image'] = $this->get_formatted_url( $image[0] );
							else:
								$this->productsList[ $i ]['feature_image'] = $this->get_formatted_url( $mainImage );
							endif;

							# Additional Images
							$imageLinks = array();
							$images     = $this->additionalImages( $post->ID );
							if ( $images && is_array( $images ) ) {
								$mKey = 1;
								foreach ( $images as $key => $value ) {
									if ( $value != $this->productsList[ $i ]['image'] ) {
										$imgLink                                 = $this->get_formatted_url( $value );
										$this->productsList[ $i ]["image_$mKey"] = $imgLink;
										if ( ! empty( $imgLink ) ) {
											array_push( $imageLinks, $imgLink );
										}
									}
									$mKey ++;
								}
							}
							$this->productsList[ $i ]['images']         = implode( ",", $imageLinks );
							$this->productsList[ $i ]['condition']      = "New";
							$this->productsList[ $i ]['type']           = $product->get_type();
							$this->productsList[ $i ]['visibility']     = $this->getAttributeValue( $post->ID, "_visibility" );
							$this->productsList[ $i ]['rating_total']   = $product->get_rating_count();
							$this->productsList[ $i ]['rating_average'] = $product->get_average_rating();
							$this->productsList[ $i ]['tags']           = $this->get_product_term_list( $post->ID, 'product_tag' );

							$this->productsList[ $i ]['item_group_id']  = $post->ID;
							$this->productsList[ $i ]['sku']            = $product->get_sku();

							$this->productsList[ $i ]['availability']   = $this->availability( $post->ID );

							$this->productsList[ $i ]['quantity']         = $this->get_quantity( $post->ID, "_stock" );
							$this->productsList[ $i ]['sale_price_sdate'] = $this->get_date( $post->ID, "_sale_price_dates_from" );
							$this->productsList[ $i ]['sale_price_edate'] = $this->get_date( $post->ID, "_sale_price_dates_to" );
							$this->productsList[ $i ]['price']            = ( $product->get_regular_price() ) ? $product->get_regular_price() : $product->get_price();
							$this->productsList[ $i ]['current_price']    = $product->get_price();
							$this->productsList[ $i ]['price_with_tax']   = ( $product->is_taxable() ) ? $this->getPriceWithTax( $product ) : $product->get_price();
							$this->productsList[ $i ]['sale_price']       = ( $product->get_sale_price() ) && ( $product->get_sale_price() > 0 ) ? $product->get_sale_price() : "";
							$this->productsList[ $i ]['weight']           = ( $product->get_weight() ) ? $product->get_weight() : "";
							$this->productsList[ $i ]['width']            = ( $product->get_width() ) ? $product->get_width() : "";
							$this->productsList[ $i ]['height']           = ( $product->get_height() ) ? $product->get_height() : "";
							$this->productsList[ $i ]['length']           = ( $product->get_length() ) ? $product->get_length() : "";

							# Sale price effective date
							$from = $this->sale_price_effective_date( $post->ID, '_sale_price_dates_from' );
							$to   = $this->sale_price_effective_date( $post->ID, '_sale_price_dates_to' );
							if ( ! empty( $from ) && ! empty( $to ) ) {
								$from                                                  = date( "c", strtotime( $from ) );
								$to                                                    = date( "c", strtotime( $to ) );
								$this->productsList[ $i ]['sale_price_effective_date'] = "$from" . "/" . "$to";
							} else {
								$this->productsList[ $i ]['sale_price_effective_date'] = "";
							}

						}
						elseif ( $type1 == 'external' ) {

							if ( $this->feedRule['is_outOfStock'] == 'y' ) {
								if ( $this->availability( $post->ID ) == "out of stock" ) {
									continue;
								}
							}

							$mainImage = wp_get_attachment_url( $product->get_image_id() );

							$getLink = new WC_Product_External( $post->ID );
							$EX_link = $getLink->get_product_url();
							$link    = get_permalink( $post->ID );
							if ( $this->feedRule['provider'] != 'custom' ) {
								if ( substr( trim( $link ), 0, 4 ) !== "http" && substr( trim( $mainImage ), 0, 4 ) !== "http" ) {
									continue;
								}
							}

							$this->productsList[ $i ]['id']             = $post->ID;
							$this->productsList[ $i ]['variation_type'] = "external";
							$this->productsList[ $i ]['title']          = $post->post_title;
							$this->productsList[ $i ]['description']    = $post->post_content;

							$this->productsList[ $i ]['short_description'] = $post->post_excerpt;
							$this->productsList[ $i ]['product_type']      = $this->get_product_term_list( $post->ID, 'product_cat', "", ">" );// $this->categories($this->parentID);//TODO
							$this->productsList[ $i ]['link']              = $link;
							$this->productsList[ $i ]['ex_link']           = $EX_link;
							$this->productsList[ $i ]['image']             = $this->get_formatted_url( $mainImage );

							# Featured Image
							if ( has_post_thumbnail( $post->ID ) ):
								$image                                     = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
								$this->productsList[ $i ]['feature_image'] = $this->get_formatted_url( $image[0] );
							else:
								$this->productsList[ $i ]['feature_image'] = $this->get_formatted_url( $mainImage );
							endif;

							# Additional Images
							$imageLinks = array();
							$images     = $this->additionalImages( $post->ID );
							if ( $images && is_array( $images ) ) {
								$mKey = 1;
								foreach ( $images as $key => $value ) {
									if ( $value != $this->productsList[ $i ]['image'] ) {
										$imgLink                                 = $this->get_formatted_url( $value );
										$this->productsList[ $i ]["image_$mKey"] = $imgLink;
										if ( ! empty( $imgLink ) ) {
											array_push( $imageLinks, $imgLink );
										}
									}
									$mKey ++;
								}
							}
							$this->productsList[ $i ]['images']         = implode( ",", $imageLinks );
							$this->productsList[ $i ]['condition']      = "New";
							$this->productsList[ $i ]['type']           = $product->get_type();
							$this->productsList[ $i ]['visibility']     = $this->getAttributeValue( $post->ID, "_visibility" );
							$this->productsList[ $i ]['rating_total']   = $product->get_rating_count();
							$this->productsList[ $i ]['rating_average'] = $product->get_average_rating();
							$this->productsList[ $i ]['tags']           = $this->get_product_term_list( $post->ID, 'product_tag' );

							$this->productsList[ $i ]['item_group_id'] = $post->ID;
							$this->productsList[ $i ]['sku']           = $product->get_sku();

							$this->productsList[ $i ]['availability'] = $this->availability( $post->ID );

							$this->productsList[ $i ]['quantity']         = $this->get_quantity( $post->ID, "_stock" );
							$this->productsList[ $i ]['sale_price_sdate'] = $this->get_date( $post->ID, "_sale_price_dates_from" );
							$this->productsList[ $i ]['sale_price_edate'] = $this->get_date( $post->ID, "_sale_price_dates_to" );
							$this->productsList[ $i ]['price']            = ( $product->get_regular_price() ) ? $product->get_regular_price() : $product->get_price();
							$this->productsList[ $i ]['current_price']    = $product->get_price();
							$this->productsList[ $i ]['price_with_tax']   = ( $product->is_taxable() ) ? $this->getPriceWithTax( $product ) : $product->get_price();
							$this->productsList[ $i ]['sale_price']       = ( $product->get_sale_price() ) && ( $product->get_sale_price() > 0 ) ? $product->get_sale_price() : "";
							$this->productsList[ $i ]['weight']           = ( $product->get_weight() ) ? $product->get_weight() : "";
							$this->productsList[ $i ]['width']            = ( $product->get_width() ) ? $product->get_width() : "";
							$this->productsList[ $i ]['height']           = ( $product->get_height() ) ? $product->get_height() : "";
							$this->productsList[ $i ]['length']           = ( $product->get_length() ) ? $product->get_length() : "";

							# Sale price effective date
							$from = $this->sale_price_effective_date( $post->ID, '_sale_price_dates_from' );
							$to   = $this->sale_price_effective_date( $post->ID, '_sale_price_dates_to' );
							if ( ! empty( $from ) && ! empty( $to ) ) {
								$from                                                  = date( "c", strtotime( $from ) );
								$to                                                    = date( "c", strtotime( $to ) );
								$this->productsList[ $i ]['sale_price_effective_date'] = "$from" . "/" . "$to";
							} else {
								$this->productsList[ $i ]['sale_price_effective_date'] = "";
							}

						}
						elseif ( $type1 == 'grouped' ) {

							if ( $this->feedRule['is_outOfStock'] == 'y' ) {
								if ( $this->availability( $post->ID ) == "out of stock" ) {
									continue;
								}
							}

							$grouped        = new WC_Product_Grouped( $post->ID );
							$children       = $grouped->get_children();
							$this->parentID = $post->ID;
							if ( $children ) {
								foreach ( $children as $cKey => $child ) {

									$product       = wc_get_product( $child );
									$this->childID = $child;
									$post          = get_post( $this->childID );

									if ( $this->feedRule['is_outOfStock'] == 'y' ) {
										if ( $this->availability( $post->ID ) == "out of stock" ) {
											continue;
										}
									}

									if ( ! empty( $this->ids_in ) && ! in_array( $post->ID, $this->ids_in ) ) {
										continue;
									}

									if ( ! empty( $this->ids_not_in ) && in_array( $post->ID, $this->ids_in ) ) {
										continue;
									}

									if ( ! $product->is_visible() ) {
										continue;
									}

									if ( $post->post_status == 'trash' ) {
										continue;
									}

									$i ++;

									$mainImage = wp_get_attachment_url( $product->get_image_id() );
									$link      = get_permalink( $post->ID );
									if ( $this->feedRule['provider'] != 'custom' ) {
										if ( substr( trim( $link ), 0, 4 ) !== "http" && substr( trim( $mainImage ), 0, 4 ) !== "http" ) {
											continue;
										}
									}

									$this->productsList[ $i ]['id']             = $post->ID;
									$this->productsList[ $i ]['variation_type'] = "grouped";
									$this->productsList[ $i ]['title']          = $post->post_title;
									$this->productsList[ $i ]['description']    = $post->post_content;

									$this->productsList[ $i ]['short_description'] = $post->post_excerpt;
									$this->productsList[ $i ]['product_type']      = $this->get_product_term_list( $post->ID, 'product_cat', "", ">" );// $this->categories($this->parentID);//TODO
									$this->productsList[ $i ]['link']              = $link;
									$this->productsList[ $i ]['ex_link']           = "";
									$this->productsList[ $i ]['image']             = $this->get_formatted_url( $mainImage );

									# Featured Image
									if ( has_post_thumbnail( $post->ID ) ):
										$image                                     = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
										$this->productsList[ $i ]['feature_image'] = $this->get_formatted_url( $image[0] );
									else:
										$this->productsList[ $i ]['feature_image'] = $this->get_formatted_url( $mainImage );
									endif;

									# Additional Images
									$imageLinks = array();
									$images     = $this->additionalImages( $this->childID );
									if ( $images && is_array( $images ) ) {
										$mKey = 1;
										foreach ( $images as $key => $value ) {
											if ( $value != $this->productsList[ $i ]['image'] ) {
												$imgLink                                 = $this->get_formatted_url( $value );
												$this->productsList[ $i ]["image_$mKey"] = $imgLink;
												if ( ! empty( $imgLink ) ) {
													array_push( $imageLinks, $imgLink );
												}
											}
											$mKey ++;
										}
									}
									$this->productsList[ $i ]['images']         = implode( ",", $imageLinks );
									$this->productsList[ $i ]['condition']      = "New";
									$this->productsList[ $i ]['type']           = $product->get_type();
									$this->productsList[ $i ]['visibility']     = $this->getAttributeValue( $post->ID, "_visibility" );
									$this->productsList[ $i ]['rating_total']   = $product->get_rating_count();
									$this->productsList[ $i ]['rating_average'] = $product->get_average_rating();
									$this->productsList[ $i ]['tags']           = $this->get_product_term_list( $post->ID, 'product_tag' );

									$this->productsList[ $i ]['item_group_id'] = $this->parentID;
									$this->productsList[ $i ]['sku']           = $product->get_sku();

									$this->productsList[ $i ]['availability'] = $this->availability( $post->ID );

									$this->productsList[ $i ]['quantity']         = $this->get_quantity( $post->ID, "_stock" );
									$this->productsList[ $i ]['sale_price_sdate'] = $this->get_date( $post->ID, "_sale_price_dates_from" );
									$this->productsList[ $i ]['sale_price_edate'] = $this->get_date( $post->ID, "_sale_price_dates_to" );
									$this->productsList[ $i ]['price']            = ( $product->get_regular_price() ) ? $product->get_regular_price() : $product->get_price();
									$this->productsList[ $i ]['current_price']    = $product->get_price();
									$this->productsList[ $i ]['price_with_tax']   = ( $product->is_taxable() ) ? $this->getPriceWithTax( $product ) : $product->get_price();
									$this->productsList[ $i ]['sale_price']       = ( $product->get_sale_price() ) && ( $product->get_sale_price() > 0 ) ? $product->get_sale_price() : "";
									$this->productsList[ $i ]['weight']           = ( $product->get_weight() ) ? $product->get_weight() : "";
									$this->productsList[ $i ]['width']            = ( $product->get_width() ) ? $product->get_width() : "";
									$this->productsList[ $i ]['height']           = ( $product->get_height() ) ? $product->get_height() : "";
									$this->productsList[ $i ]['length']           = ( $product->get_length() ) ? $product->get_length() : "";

									# Sale price effective date
									$from = $this->sale_price_effective_date( $post->ID, '_sale_price_dates_from' );
									$to   = $this->sale_price_effective_date( $post->ID, '_sale_price_dates_to' );
									if ( ! empty( $from ) && ! empty( $to ) ) {
										$from                                                  = date( "c", strtotime( $from ) );
										$to                                                    = date( "c", strtotime( $to ) );
										$this->productsList[ $i ]['sale_price_effective_date'] = "$from" . "/" . "$to";
									} else {
										$this->productsList[ $i ]['sale_price_effective_date'] = "";
									}
								}
							}
						}
						else if ( $type1 == 'variable' && $product->has_child() ) {


							if ( $this->feedRule['is_variations'] == 'n' ) {

								if ( $this->feedRule['is_outOfStock'] == 'y' ) {
									if ( $this->availability( $post->ID ) == "out of stock" ) {
										continue;
									}
								}

								$mainImage = wp_get_attachment_url( $product->get_image_id() );
								$link      = get_permalink( $post->ID );

								if ( $this->feedRule['provider'] != 'custom' ) {
									if ( substr( trim( $link ), 0, 4 ) !== "http" && substr( trim( $mainImage ), 0, 4 ) !== "http" ) {
										continue;
									}
								}

								$this->productsList[ $i ]['id']             = $post->ID;
								$this->productsList[ $i ]['variation_type'] = "parent";
								$this->productsList[ $i ]['title']          = $post->post_title;
								$this->productsList[ $i ]['description']    = $post->post_content;

								$this->productsList[ $i ]['short_description'] = $post->post_excerpt;
								$this->productsList[ $i ]['product_type']      = $this->get_product_term_list( $post->ID, 'product_cat', "", ">" );// $this->categories($this->parentID);//TODO
								$this->productsList[ $i ]['link']              = $link;
								$this->productsList[ $i ]['ex_link']           = "";
								$this->productsList[ $i ]['image']             = $this->get_formatted_url( $mainImage );

								# Featured Image
								if ( has_post_thumbnail( $post->ID ) ):
									$image                                     = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
									$this->productsList[ $i ]['feature_image'] = $this->get_formatted_url( $image[0] );
								else:
									$this->productsList[ $i ]['feature_image'] = $this->get_formatted_url( $mainImage );
								endif;

								# Additional Images
								$imageLinks = array();
								$images     = $this->additionalImages( $this->childID );
								if ( $images && is_array( $images ) ) {
									$mKey = 1;
									foreach ( $images as $key => $value ) {
										if ( $value != $this->productsList[ $i ]['image'] ) {
											$imgLink                                 = $this->get_formatted_url( $value );
											$this->productsList[ $i ]["image_$mKey"] = $imgLink;
											if ( ! empty( $imgLink ) ) {
												array_push( $imageLinks, $imgLink );
											}
										}
										$mKey ++;
									}
								}
								$this->productsList[ $i ]['images'] = implode( ",", $imageLinks );

								$this->productsList[ $i ]['condition']      = "New";
								$this->productsList[ $i ]['type']           = $product->get_type();
								$this->productsList[ $i ]['visibility']     = $this->getAttributeValue( $post->ID, "_visibility" );
								$this->productsList[ $i ]['rating_total']   = $product->get_rating_count();
								$this->productsList[ $i ]['rating_average'] = $product->get_average_rating();
								$this->productsList[ $i ]['tags']           = $this->get_product_term_list( $post->ID, 'product_tag' );

								$this->productsList[ $i ]['item_group_id'] = $post->ID;
								$this->productsList[ $i ]['sku']           = $product->get_sku();

								$this->productsList[ $i ]['availability'] = $this->availability( $post->ID );

								$this->productsList[ $i ]['quantity']         = $this->get_quantity( $post->ID, "_stock" );
								$this->productsList[ $i ]['sale_price_sdate'] = $this->get_date( $post->ID, "_sale_price_dates_from" );
								$this->productsList[ $i ]['sale_price_edate'] = $this->get_date( $post->ID, "_sale_price_dates_to" );


								if ( $this->feedRule['variable_price'] == 'first' ) {
									$price  = ( $product->get_price() ) ? $product->get_price() : "";
									$sPrice = ( $product->get_sale_price() ) ? $product->get_sale_price() : "";
								} else {
									$getVariable = new WC_Product_Variable( $product );
									$price       = $getVariable->get_variation_regular_price( $this->feedRule['variable_price'], $product->is_taxable() );
									$sPrice      = $getVariable->get_variation_sale_price( $this->feedRule['variable_price'], $product->is_taxable() );
								}


								$this->productsList[ $i ]['price']          = ( $price ) ? $price : $product->get_price();
								$this->productsList[ $i ]['current_price']    = $product->get_price();
								$this->productsList[ $i ]['price_with_tax'] = ( $product->is_taxable() ) ? $this->getPriceWithTax( $product ) : $price;
								$this->productsList[ $i ]['sale_price']     = ( $sPrice ) && ( $sPrice > 0 ) ? $sPrice : "";
								$this->productsList[ $i ]['weight']         = ( $product->get_weight() ) ? $product->get_weight() : "";
								$this->productsList[ $i ]['width']          = ( $product->get_width() ) ? $product->get_width() : "";
								$this->productsList[ $i ]['height']         = ( $product->get_height() ) ? $product->get_height() : "";
								$this->productsList[ $i ]['length']         = ( $product->get_length() ) ? $product->get_length() : "";

								# Sale price effective date
								$from = $this->sale_price_effective_date( $post->ID, '_sale_price_dates_from' );
								$to   = $this->sale_price_effective_date( $post->ID, '_sale_price_dates_to' );
								if ( ! empty( $from ) && ! empty( $to ) ) {
									$from                                                  = date( "c", strtotime( $from ) );
									$to                                                    = date( "c", strtotime( $to ) );
									$this->productsList[ $i ]['sale_price_effective_date'] = "$from" . "/" . "$to";
								} else {
									$this->productsList[ $i ]['sale_price_effective_date'] = "";
								}

							}

							if ( $this->feedRule['is_variations'] == 'y' ) {
								# Get Child Products
								if ( $product->has_child() ) {
									$variations = $product->get_available_variations();

									if ( ! empty( $variations ) ) {
										foreach ( $variations as $key => $variation ) {

											$status = get_post( $this->childID );

											if ( ! $status || ! is_object( $status ) ) {
												continue;
											}

											if ( $status->post_status == "trash" ) {
												continue;
											}

											$this->childID  = $variation['variation_id'];
											$this->parentID = wp_get_post_parent_id( $this->childID );

											if ( $this->feedRule['is_outOfStock'] == 'y' ) {
												if ( $this->availability( $this->childID ) == "out of stock" ) {
													continue;
												}
											}

											if ( ! empty( $this->ids_in ) && ! in_array( $variation['variation_id'], $this->ids_in ) ) {
												continue;
											}

											if ( ! empty( $this->ids_not_in ) && in_array( $variation['variation_id'], $this->ids_in ) ) {
												continue;
											}

											if ( ! $variation['variation_is_visible'] ) {
												continue;
											}

											$i ++;

											$product   = wc_get_product( $this->childID );
											$mainImage = wp_get_attachment_url( $product->get_image_id() );

											if ( ! $mainImage ) {
												$image     = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
												$mainImage = $this->get_formatted_url( $image[0] );
											}
											$link = $product->get_permalink( $variation['variation_id'] );
											if ( substr( trim( $link ), 0, 4 ) !== "http" ) {
												$link = get_permalink( $this->parentID );
											}

											$this->productsList[ $i ]['id']             = $this->childID;
											$this->productsList[ $i ]['variation_type'] = "child";
											$this->productsList[ $i ]['item_group_id']  = $this->parentID;
											$this->productsList[ $i ]['sku']            = $product->get_sku();
											$this->productsList[ $i ]['parent_sku']     = $this->getAttributeValue( $this->parentID, "_sku" );
											$this->productsList[ $i ]['title']          = $post->post_title;
											$this->productsList[ $i ]['description']    =$post->post_content;

											# Short Description to variable description
											$vDesc = $this->getAttributeValue( $this->childID, "_variation_description" );

											if ( ! empty( $vDesc ) ) {
												$this->productsList[ $i ]['short_description'] = $vDesc;
											} else {
												$this->productsList[ $i ]['short_description'] = $post->post_excerpt;
											}

											$this->productsList[ $i ]['product_type'] = $this->get_product_term_list( $post->ID, 'product_cat', "", ">" );// $this->categories($this->parentID);//TODO
											$this->productsList[ $i ]['link']         = $link;
											$this->productsList[ $i ]['ex_link']      = "";
											$this->productsList[ $i ]['image']        = $this->get_formatted_url( $mainImage );

											# Featured Image
											if ( has_post_thumbnail( $post->ID ) ):
												$image                                     = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
												$this->productsList[ $i ]['feature_image'] = $this->get_formatted_url( $image[0] );
											else:
												$this->productsList[ $i ]['feature_image'] = $this->get_formatted_url( $mainImage );
											endif;

//                                        # Additional Images
//                                        $imageLinks=array();
//                                        $images = $this->additionalImages($this->parentID);
//                                        if ($images and is_array($images)) {
//                                            foreach ($images as $key => $value) {
//                                                if ($value != $this->productsList[$i]['image']) {
//                                                    $imgLink=$this->get_formatted_url($value);
//                                                    $this->productsList[$i]["image_$key"] = $imgLink;
//                                                    if(!empty($imgLink)){
//                                                        array_push($imageLinks,$imgLink);
//                                                    }
//                                                }
//                                            }
//                                        }

											# Additional Images
											$imageLinks = array();
											$images     = $this->additionalImages( $this->parentID );
											if ( $images && is_array( $images ) ) {
												$mKey = 1;
												foreach ( $images as $key => $value ) {
													if ( $value != $this->productsList[ $i ]['image'] ) {
														$imgLink                                 = $this->get_formatted_url( $value );
														$this->productsList[ $i ]["image_$mKey"] = $imgLink;
														if ( ! empty( $imgLink ) ) {
															array_push( $imageLinks, $imgLink );
														}
													}
													$mKey ++;
												}
											}

											$this->productsList[ $i ]['images']         = implode( ",", $imageLinks );
											$this->productsList[ $i ]['condition']      = "New";
											$this->productsList[ $i ]['type']           = $product->get_type();
											$this->productsList[ $i ]['visibility']     = $this->getAttributeValue( $this->childID, "_visibility" );
											$this->productsList[ $i ]['rating_total']   = $product->get_rating_count();
											$this->productsList[ $i ]['rating_average'] = $product->get_average_rating();
											$this->productsList[ $i ]['tags']           = $this->get_product_term_list( $post->ID, 'product_tag' );
											$this->productsList[ $i ]['shipping']       = $product->get_shipping_class();

											$this->productsList[ $i ]['availability']     = $this->availability( $this->childID );
											$this->productsList[ $i ]['quantity']         = ( $this->get_quantity( $this->childID, "_stock" ) );
											$this->productsList[ $i ]['sale_price_sdate'] = $this->get_date( $this->childID, "_sale_price_dates_from" );
											$this->productsList[ $i ]['sale_price_edate'] = $this->get_date( $this->childID, "_sale_price_dates_to" );
											$this->productsList[ $i ]['price']            = ( $product->get_regular_price() ) ? $product->get_regular_price() : $product->get_price();
											$this->productsList[ $i ]['current_price']    = $product->get_price();
											$this->productsList[ $i ]['price_with_tax']   = ( $product->is_taxable() ) ? $this->getPriceWithTax( $product ) : $product->get_price();
											$this->productsList[ $i ]['sale_price']       = ( $product->get_sale_price() ) && ( $product->get_sale_price() > 0 ) ? $product->get_sale_price() : "";
											$this->productsList[ $i ]['weight']           = ( $product->get_weight() ) ? $product->get_weight() : "";
											$this->productsList[ $i ]['width']            = ( $product->get_width() ) ? $product->get_width() : "";
											$this->productsList[ $i ]['height']           = ( $product->get_height() ) ? $product->get_height() : "";
											$this->productsList[ $i ]['length']           = ( $product->get_length() ) ? $product->get_length() : "";

											# Sale price effective date
											$from = $this->sale_price_effective_date( $this->childID, '_sale_price_dates_from' );
											$to   = $this->sale_price_effective_date( $this->childID, '_sale_price_dates_to' );
											if ( ! empty( $from ) && ! empty( $to ) ) {
												$from                                                  = date( "c", strtotime( $from ) );
												$to                                                    = date( "c", strtotime( $to ) );
												$this->productsList[ $i ]['sale_price_effective_date'] = "$from" . "/" . "$to";
											} else {
												$this->productsList[ $i ]['sale_price_effective_date'] = "";
											}
										}
									}
								}
							}
						}
					}
					$i ++;

				endwhile;
				wp_reset_query();
				if ( $this->WPFP_WPML ) {
					if ( isset( $feedRule['feedLanguage'] ) && $feedRule['feedLanguage'] != $this->currentLang ) {
						do_action( 'wpml_switch_language', $this->currentLang );
					}
				}

				return $this->productsList;
			} catch ( Exception $e ) {
				if ( $this->WPFP_WPML ) {
					if ( isset( $feedRule['feedLanguage'] ) && $feedRule['feedLanguage'] != $this->currentLang ) {
						do_action( 'wpml_switch_language', $this->currentLang );
					}
				}

				//die( $e->getMessage() );
			}

		}
	}



	/** Return product price with tax
	 * @param $product
	 * @return float|string
	 */
	public function getPriceWithTax($product)
	{
		if(woo_feed_wc_version_check(3.0)){
			return wc_get_price_including_tax($product,array('price'=>$product->get_price()));
		}else{
			return $product->get_price_including_tax();
		}
	}

	/**
	 * @param object $product
	 *
	 * @return string
	 */
	public function get_formatted_title($product) {
		$name = $product->get_name();
		$attributes=$product->get_attributes();
		$val=array();
		if(!empty($attributes)){
			foreach ($attributes as $key=>$attribute){
				$attr=$product->get_attribute($key);
				if(!empty($attr)){
					$val[$key]=$product->get_attribute($key);
				}
			}
		}

		if(!empty($val)){
			return $name." - ".implode(", ",$val);
		}else{
			return $name;
		}
	}

	/**
	 * Get formatted image url
	 *
	 * @param $url
	 * @return bool|string
	 */
	public function get_formatted_url($url = "")
	{
		if (!empty($url)) {
			if (substr(trim($url), 0, 4) === "http" || substr(trim($url), 0, 3) === "ftp" || substr(trim($url), 0, 4) === "sftp") {
				return rtrim($url, "/");
			} else {
				$base = get_site_url();
				$url = $base . $url;
				return rtrim($url, "/");
			}
		}
		return "";
	}


	/**
	 * Get formatted product date
	 *
	 * @param $id
	 * @param $name
	 * @return bool|string
	 */
	public function get_date($id, $name)
	{
		$date = $this->getAttributeValue($id, $name);
		if ($date) {
			return date("Y-m-d", $date);
		}
		return false;
	}

	/**
	 * Get formatted product quantity
	 *
	 * @param $id
	 * @param $name
	 * @return bool|mixed
	 */
	public function get_quantity($id, $name)
	{
		$qty=array();
		$product=wc_get_product($id);
		if($product->is_type("variable") && $product->has_child()){
			$variable=new WC_Product_Variable($id);
			$children=$variable->get_available_variations();
			if(!empty($children)){
				foreach($children as $child){
					$qty[]=$child['max_qty'];
				}
			}

			if(isset($this->feedRule['variable_quantity'])){
				$vaQty=$this->feedRule['variable_quantity'];
				if($vaQty=="max"){
					return max($qty)+0;
				}elseif ($vaQty=="min"){
					return min($qty)+0;
				}elseif ($vaQty=="sum"){
					return array_sum($qty)+0;
				}elseif ($vaQty=="first"){
					return $qty[0]+0;
				}else{
					$qty = $this->getProductMeta($id, $name);
					if ($qty) {
						return $qty + 0;
					}
				}
			}

		}else{
			$qty = $this->getProductMeta($id, $name);
			if ($qty) {
				return $qty + 0;
			}
		}

		return "0";
	}

	/**
	 * Retrieve a post's terms as a list with specified format.
	 *
	 * @since 2.5.0
	 *
	 * @param int $id Post ID.
	 * @param string $taxonomy Taxonomy name.
	 * @param string $before Optional. Before list.
	 * @param string $sep Optional. Separate items using this.
	 * @param string $after Optional. After list.
	 *
	 * @return string|false|WP_Error A list of terms on success, false if there are no terms, WP_Error on failure.
	 */
	function get_product_term_list($id, $taxonomy, $before = '', $sep = ',', $after = '')
	{
		$terms = get_the_terms($id, $taxonomy);

		if (is_wp_error($terms)) {
			return $terms;
		}

		if (empty($terms)) {
			return false;
		}

		$links = array();

		foreach ($terms as $term) {
			$links[$term->term_id] = $term->name;
		}
		ksort($links);
		return $before . join($sep, $links) . $after;
	}

	/** Return additional image URLs
	 *
	 * @param integer $productId
	 *
	 * @return array
	 */
	public function additionalImages($productId)
	{
		$ids=$this->getAttributeValue($productId,"_product_image_gallery");
		$imgIds=!empty($ids)?explode(",",$ids):"";

		$images = array();
		if (!empty($imgIds)) {
			foreach ($imgIds as $key => $value) {
				if ($key < 10) {
					$images[$key] = wp_get_attachment_url($value);
				}
			}
			return $images;
		}
		return false;
	}

	/**
	 * Give space to availability text
	 *
	 * @param integer $id
	 *
	 * @return string
	 */
	public function availability($id)
	{
		$status=$this->getAttributeValue($id,"_stock_status");
		if ($status) {
			if ($status == 'instock') {
				return "in stock";
			} elseif ($status == 'outofstock') {
				return "out of stock";
			}
		}
		return "out of stock";
	}

	/**
	 * Ger Product Attribute
	 *
	 * @since 2.2.3
	 * @param $id
	 * @param $attr
	 *
	 * @return string
	 */
	public function getProductAttribute($id,$attr) {

		$attr=str_replace("wf_attr_", "",$attr);

		if(woo_feed_wc_version_check(3.2)){
			# Get Product
			$product=wc_get_product($id);

			if(!is_object($product)){
				return "";
			}
			$value=$product->get_attribute($attr);

			return $value;
		}else{
			return implode(',', wc_get_product_terms($id,$attr, array('fields' => 'names')));
		}
	}


	/**
	 * Get Meta
	 *
	 * @since 2.2.3
	 * @param $id
	 * @param $meta
	 *
	 * @return mixed|string
	 */

	public function getProductMeta($id,$meta) {

		$meta=str_replace("wf_cattr_", "",$meta);

		if (strpos($meta, 'attribute_pa') !== false) {
			$productMeta= $this->getProductAttribute($id,str_replace("attribute_","",$meta));
		}else{
			$productMeta=  get_post_meta($id, $meta, true);
		}

		if(empty($productMeta) && class_exists("SitePress") && strpos($meta, '_price_') !== false){

			global $sitepress;
			$lang= $sitepress->get_default_language();

			if(woo_feed_wc_version_check(3.2)){
				$parentId=apply_filters( 'wpml_object_id',$id, 'product', false,$lang);
			}else{
				$parentId=icl_object_id($id);
			}

			$productMeta=  get_post_meta($parentId, $meta, true);
		}

		return $productMeta;
	}

	/**
	 * Get Taxonomy
	 *
	 * @param $id
	 * @param $taxonomy
	 *
	 * @return string
	 */
	public function getProductTaxonomy( $id, $taxonomy ) {
		$taxonomy=str_replace("wf_taxo_", "",$taxonomy);
		$Taxo=get_the_term_list($id,$taxonomy,"",",","");

		if(!empty($Taxo)){
			return strip_tags($Taxo);
		}

		return "";
	}

	/**
	 * Get Product Attribute Value or Post Meta value
	 * @deprecated 2.2.3
	 * @param $id
	 * @param $name
	 *
	 * @return mixed
	 */
	public function getAttributeValue($id, $name)
	{
		return $this->getProductMeta($id,$name);

//		if (strpos($name, 'attribute_pa') !== false) {
//			$slug=get_post_meta($id, $name, true);
//			$value=get_term_by('slug',$slug,str_replace('attribute_',"",$name));
//			if($value){
//				return $value->name;
//			}
//		}else{
//			if(get_post_meta($id, $name, true)){
//				return get_post_meta($id, $name, true);
//			}elseif(strpos($name,'_price_')!==FALSE){
//				return get_post_meta($id, $name, true);
//			}
//		}
//		return false;
	}



	/**
	 * Get Sale price effective date for google
	 *
	 * @param $id
	 * @param $name
	 * @return string
	 */
	public function sale_price_effective_date($id, $name)
	{
		return ($date = $this->getAttributeValue($id, $name)) ? date_i18n('Y-m-d', $date) : "";
	}


	/**
	 * Get All Default WooCommerce Attributes
	 * @return bool|array
	 */
	public function getAllAttributes()
	{
		global $wpdb;

		//Load the main attributes
		$sql = '
			SELECT attribute_name as name, attribute_type as type
			FROM ' . $wpdb->prefix . 'woocommerce_attribute_taxonomies';
		$data = $wpdb->get_results($sql);
		if (count($data)) {
			foreach ($data as $key => $value) {
				$info["wf_attr_pa_" . $value->name] = $value->name;
			}
			return $info;
		}
		return false;
	}


	/**
	 * Get All Custom Attributes
	 * @return array|bool
	 */
	public function getAllCustomAttributes()
	{
		global $wpdb;
		$info = array();
		//Load the main attributes
		$sql = "SELECT meta_key as name, meta_value as type
			FROM " . $wpdb->prefix . "postmeta" . "  group by meta_key";
		$data = $wpdb->get_results($sql);
		if (count($data)) {
			foreach ($data as $key => $value) {
				$info["wf_cattr_" . $value->name] = $value->name;
			}
			return $info;
		}
		return false;
	}

	/**
	 * Get All Taxonomy
	 *
	 * @param string $name
	 *
	 * @return mixed
	 */
	public function getAllTaxonomy($name = "color")
	{
		global $wpdb;
		//Load the taxonomies
		$info = false;

		$sql = "SELECT taxo.taxonomy, terms.name, terms.slug FROM $wpdb->term_taxonomy taxo
			LEFT JOIN $wpdb->terms terms ON (terms.term_id = taxo.term_id) GROUP BY taxo.taxonomy";
		$data = $wpdb->get_results($sql);
		if (count($data)) {
			foreach ($data as $key => $value) {
				$info["wf_taxo_" . $value->taxonomy] = $value->taxonomy;
			}
		}

		return $info;
	}

	/**
	 * Get Category Mappings
	 * @return bool|array
	 */
	public function getCustomCategoryMappedAttributes()
	{
		global $wpdb;

		//Load Custom Category Mapped Attributes
		$var = "wf_cmapping_";
		$sql = $wpdb->prepare("SELECT * FROM $wpdb->options WHERE option_name LIKE %s;", $var . "%");
		$data = $wpdb->get_results($sql);
		if (count($data)) {
			foreach ($data as $key => $value) {
				$info[$key] = $value->option_name;
			}
			return $info;
		}

		return false;
	}

	/**
	 * Get Dynamic Attribute List
	 * @return bool|array
	 */
	public function dynamicAttributes()
	{
		global $wpdb;

		# Load Custom Category Mapped Attributes
		$var = "wf_dattribute_";
		$sql = $wpdb->prepare("SELECT * FROM $wpdb->options WHERE option_name LIKE %s;", $var . "%");
		$data = $wpdb->get_results($sql);
		if (count($data)) {
			foreach ($data as $key => $value) {
				$info[$key] = $value->option_name;
			}
			return $info;
		}
		return false;
	}

	public function load_attributes()
	{
		# Get All WooCommerce Attributes
		$vAttributes = $this->getAllAttributes();
		update_option("wpfw_vAttributes", $vAttributes);

		# Get All Custom Attributes
		$customAttributes = $this->getAllCustomAttributes();
		update_option("wpfw_customAttributes", $customAttributes);

		# Get All Custom Taxonomies
		$customTaxonomy = $this->getAllTaxonomy();
		update_option("wpfw_customTaxonomy", $customTaxonomy);

		# Dynamic Attribute List
		$dynamicAttributes = $this->dynamicAttributes();
		update_option("wpfw_dynamicAttributes", $dynamicAttributes);

		# Category Mapping List
		$categoryMapping = $this->getCustomCategoryMappedAttributes();
		update_option("wpfw_categoryMapping", $categoryMapping);
	}

	/**
	 * Local Attribute List to map product value with merchant attributes
	 *
	 * @param string $selected
	 *
	 * @return string
	 */
	public function attributeDropdown($selected = "")
	{
		$attributes = array(
			"id" => "Product Id",
			"title" => "Product Title",
			"description" => "Product Description",
			"short_description" => "Product Short Description",
			"product_type" => "Product Local Category",
			"link" => "Product URL",
			"ex_link" => "External Product URL",
			"condition" => "Condition",
			"item_group_id" => "Parent Id [Group Id]",
			"sku" => "SKU",
			"parent_sku" => "Parent SKU",
			"availability" => "Availability",
			"quantity" => "Quantity",
			"price" => "Regular Price",
			"current_price" => "Current Price",
			"price_with_tax" => "Price With Tax",
			"sale_price" => "Sale Price",
			"sale_price_sdate" => "Sale Start Date",
			"sale_price_edate" => "Sale End Date",
			"weight" => "Weight",
			"width" => "Width",
			"height" => "Height",
			"length" => "Length",
			"shipping_class" => "Shipping Class",
			"type" => "Product Type",
			"variation_type" => "Variation Type",
			"visibility" => "Visibility",
			"rating_total" => "Total Rating",
			"rating_average" => "Average Rating",
			"tags" => "Tags",
			"sale_price_effective_date" => "Sale Price Effective Date",
			"is_bundle" => "Is Bundle",

		);

		$images = array(
			"image" => "Main Image",
			"feature_image" => "Featured Image",
			"images" => "Images [Comma Separated]",
			"image_1" => "Additional Image 1",
			"image_2" => "Additional Image 2",
			"image_3" => "Additional Image 3",
			"image_4" => "Additional Image 4",
			"image_5" => "Additional Image 5",
			"image_6" => "Additional Image 6",
			"image_7" => "Additional Image 7",
			"image_8" => "Additional Image 8",
			"image_9" => "Additional Image 9",
			"image_10" => "Additional Image 10",
		);

		# Primary Attributes
		$str = "<option></option>";
		$sltd = "";
		$str .= "<optgroup label='Primary Attributes'>";
		foreach ($attributes as $key => $value) {
			$sltd = "";
			if ($selected == $key) {
				$sltd = 'selected="selected"';
			}
			$str .= "<option $sltd value='$key'>" . $value . "</option>";
		}
		$str .= "</optgroup>";

		# Additional Images
		if ($images) {
			$str .= "<optgroup label='Image Attributes'>";
			foreach ($images as $key => $value) {
				$sltd = "";
				if ($selected == $key) {
					$sltd = 'selected="selected"';
				}
				$str .= "<option $sltd value='$key'>" . $value . "</option>";
			}
			$str .= "</optgroup>";
		}

		# Get All WooCommerce Attributes
		$vAttributes = get_option("wpfw_vAttributes");
		if ($vAttributes) {
			$str .= "<optgroup label='Product Attributes'>";
			foreach ($vAttributes as $key => $value) {
				$sltd = "";
				if ($selected == $key) {
					$sltd = 'selected="selected"';
				}
				$str .= "<option $sltd value='$key'>" . $value . "</option>";
			}
			$str .= "</optgroup>";
		}

		# Get All Custom Attributes
		$customAttributes = get_option("wpfw_customAttributes");
		if ($customAttributes) {
			$str .= "<optgroup label='Variation & Custom Attributes'>";
			foreach ($customAttributes as $key => $value) {
				$sltd = "";
				if ($selected == $key) {
					$sltd = 'selected="selected"';
				}
				$str .= "<option $sltd value='$key'>" . $value . "</option>";
			}
			$str .= "</optgroup>";
		}


		if (get_option('woocommerce_product_feed_pro_activated') && get_option('woocommerce_product_feed_pro_activated') == "Activated") {

			# Get All Custom Taxonomies
			$customTaxonomy = get_option("wpfw_customTaxonomy");
			if ($customTaxonomy) {
				$str .= "<optgroup label='Custom Taxonomies'>";
				foreach ($customTaxonomy as $key => $value) {
					$sltd = "";
					if ($selected == $key) {
						$sltd = 'selected="selected"';
					}
					$str .= "<option $sltd value='$key'>" . $value . "</option>";
				}
				$str .= "</optgroup>";
			}

			# Get all saved option
			$wp_options = get_option("wpfp_option");
			if ($wp_options && count($wp_options)) {
				$str .= "<optgroup label='WP Options'>";
				foreach ($wp_options as $key => $option) {
					$sltd = "";
					$name = $option['option_name'];
					$newName = str_replace('wf_option_', '', $name);
					if ($selected == $name) {
						$sltd = 'selected="selected"';
					}
					$str .= "<option $sltd value=$name>" . $newName . "</option>";
				}
				$str .= "</optgroup>";
			}

			# Dynamic Attribute List
			$dynamicAttributes = get_option("wpfw_dynamicAttributes");
			if (count($dynamicAttributes) and is_array($dynamicAttributes)) {
				$str .= "<optgroup label='Dynamic Attributes'>";
				foreach ($dynamicAttributes as $key => $value) {
					$sltd = "";
					$newValue = str_replace('wf_dattribute_', '', $value);
					if ($selected == $value) {
						$sltd = 'selected="selected"';
					}
					$str .= "<option $sltd value='$value'>" . $newValue . "</option>";
				}
				$str .= "</optgroup>";
			}

			# Category Mapping List
			$categoryMapping = get_option("wpfw_categoryMapping");
			if (count($categoryMapping) and is_array($categoryMapping)) {
				$str .= "<optgroup label='Category Mapping Attributes'>";
				foreach ($categoryMapping as $key => $value) {
					$sltd = "";
					$newValue = str_replace('wf_cmapping_', '', $value);
					if ($selected == $value) {
						$sltd = 'selected="selected"';
					}
					$str .= "<option $sltd value='$value'>" . $newValue . "</option>";
				}
				$str .= "</optgroup>";
			}
		}
		return $str;
	}


}